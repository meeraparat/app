//
//  GameOver19.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver19.h"
#import "Level19.h"

@implementation GameOver19

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level19 alloc] initWithLevel19]];
}

@end
