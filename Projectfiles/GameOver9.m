//
//  GameOver9.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver9.h"
#import "Level9.h"

@implementation GameOver9

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level9 alloc] initWithLevel9]];
}


@end
