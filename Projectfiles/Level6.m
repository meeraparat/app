//
//  Level6.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 7/31/13.
//
//

#import "Level6.h"
#import "NextLevel67.h"
#import "GameOver6.h"

@implementation Level6


static Level6 * instanceOfLevel6;


//If another class wants to get a reference to this layer, they can by calling this method
+(Level6 *)sharedLevel6
{
	NSAssert(instanceOfLevel6 != nil, @"Level6 instance not yet initialized!");
	return instanceOfLevel6;
}

-(id) initWithLevel6
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateSix:)];
        [self level6mods];
    }
    return self;
}

-(void) level6mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel6 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(251, 54, 40, 250)];
    [self addChild:colorLayer z:0];
    
    rightButton.position = ccp(-width, -(height * 10));
    leftButton.position = ccp(-width, -(height * 10));
    
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Tap on the objects" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 6" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(55, (height - 25));
    [self addChild:levelDisplay];

 
}

-(void) updateSix: (ccTime) dt
{
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel67 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver6 alloc] init]];
}


-(void) processRightCollisions: (NSMutableArray *) arrayEntry
{
    for (int i = 0; i < [arrayEntry count]; i++)
    {
        CGRect Box = CGRectMake(([[rightFallingObjects objectAtIndex:i] boundingBox].origin.x - [[rightFallingObjects objectAtIndex:i] contentSize].width/2 - 5), ([[rightFallingObjects objectAtIndex:i] boundingBox].origin.y - 5), ([[rightFallingObjects objectAtIndex:i] contentSize].width + 10), ([[rightFallingObjects objectAtIndex:i]contentSize].height + 10));
        
        if ((CGRectContainsPoint(Box, pos)) )
        {
            ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
            [arrayEntry removeObjectAtIndex:i];
            score = score + 10;
        }
    }
}


-(void) processLeftCollisions: (NSMutableArray *) arrayEntry
{
    for (int i = 0; i < [arrayEntry count]; i++)
    {
            CGRect Box = CGRectMake(([[leftFallingObjects objectAtIndex:i] boundingBox].origin.x - [[leftFallingObjects objectAtIndex:i] contentSize].width/2 - 5), ([[leftFallingObjects objectAtIndex:i] boundingBox].origin.y - 5), ([[leftFallingObjects objectAtIndex:i] contentSize].width + 10), ([[leftFallingObjects objectAtIndex:i]contentSize].height + 10));
            
            
        if ((CGRectContainsPoint(Box, pos)) )
        {
            ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
            [arrayEntry removeObjectAtIndex:i];
            score = score + 10;
        }
    }
}


@end
