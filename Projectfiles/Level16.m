//
//  Level16.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/5/13.
//
//

#import "Level16.h"
#import "NextLevel1617.h"
#import "GameOver16.h"

@implementation Level16


static Level16 * instanceOfLevel16;


//If another class wants to get a reference to this layer, they can by calling this method
+(Level16*) sharedLevel16
{
	NSAssert(instanceOfLevel16 != nil, @"Level16 instance not yet initialized!");
	return instanceOfLevel16;
}

-(id) initWithLevel16
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateSixteen:)];
        [self level16mods];
    }
    return self;
}

-(void) level16mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel16 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(250, 252, 120, 250)];
    [self addChild:colorLayer z:0];
        
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Tap on the Red ones" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 16" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(50, (height - 30));
    [self addChild:levelDisplay];
}


-(void) updateSixteen: (ccTime) dt
{
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel1617 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver16 alloc] init]];
}

-(void) moveFallingObjects: (NSMutableArray*) fallingObjectArray
{
    for(int i = 0; i < [fallingObjectArray count]; i++)
    {
        [[fallingObjectArray objectAtIndex:i] setPosition: ccp([(CCSprite*)[fallingObjectArray objectAtIndex:i] position].x, [(CCSprite*)[fallingObjectArray objectAtIndex:i] position].y - 2.5)];
    }
}

-(void) processRightCollisions: (NSMutableArray*) arrayEntry
{
    CGRect RightRect = CGRectMake(rightButton.position.x - width/2, height/2 + rightButton.position.y, rightButton.contentSize.width, rightButton.contentSize.height);
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isRightClicked == TRUE)
        {
            if([[arrayEntry objectAtIndex:i] tag] == 0)
            {
                if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (RightRect.origin.y  + RightRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (RightRect.origin.y - RightRect.size.height/2 - 5)))
                {
                    ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                    [arrayEntry removeObjectAtIndex:i];
                    score = score + 10;
                }
            }
            if (([[arrayEntry objectAtIndex:i] tag] == 1) || ([[arrayEntry objectAtIndex:i] tag] == 2))
            {
                if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (RightRect.origin.y  + RightRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (RightRect.origin.y - RightRect.size.height/2 - 5)))
                {
                    score = score - 10;
                    CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                    CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:250 green:252 blue:120];
                    CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                    [colorLayer runAction: sequence];
                }
            }
        }
    }
    isRightClicked = FALSE;
}


-(void) processLeftCollisions: (NSMutableArray*) arrayEntry
{
    
    CGRect LeftRect = CGRectMake(leftButton.position.x - width/2, height/2 + leftButton.position.y, leftButton.contentSize.width, leftButton.contentSize.height);
    
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isLeftClicked == TRUE)
        {
            if([[arrayEntry objectAtIndex:i] tag] == 0)
            {
                if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (LeftRect.origin.y  + LeftRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (LeftRect.origin.y - LeftRect.size.height/2 - 5)))
                {
                    ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                    [arrayEntry removeObjectAtIndex:i];
                    score = score + 10;
                }
            }
            if (([[arrayEntry objectAtIndex:i] tag] == 1) || ([[arrayEntry objectAtIndex:i] tag] == 2))
            {
                if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (LeftRect.origin.y  + LeftRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (LeftRect.origin.y - LeftRect.size.height/2 - 5)))
                {
                    score = score - 10;
                    CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                    CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:250 green:252 blue:120];
                    CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                    [colorLayer runAction: sequence];
                }
            }
        }
    }
    isLeftClicked = FALSE;
}



@end
