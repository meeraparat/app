//
//  Level10.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/1/13.
//
//

#import "Level10.h"
#import "NextLevel1011.h"
#import "GameOver10.h"

@implementation Level10

static Level10 * instanceOfLevel10;



//If another class wants to get a reference to this layer, they can by calling this method
+(Level10*) sharedLevel10
{
	NSAssert(instanceOfLevel10 != nil, @"Level10 instance not yet initialized!");
	return instanceOfLevel10;
}

-(id) initWithLevel10
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateTen:)];
        [self level10mods];
    }
    return self;
}

-(void) level10mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel10 = self;
    
    rightButton.position = ccp(-width, -(height * 10));
    leftButton.position = ccp(-width, -(height * 10));
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(253, 160, 239, 250)];
    [self addChild:colorLayer z:0];
        
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Freeby - click anywhere" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 10" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(55, (height - 25));
    [self addChild:levelDisplay];
    
}

-(void) updateTen: (ccTime) dt
{
    if (touchCounter == 5)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel1011 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver10 alloc] init]];
}


@end
