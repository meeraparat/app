//
//  Level14.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/4/13.
//
//

#import "Level14.h"
#import "NextLevel1415.h"
#import "GameOver14.h"

@implementation Level14

static Level14 * instanceOfLevel14;



//If another class wants to get a reference to this layer, they can by calling this method
+(Level14*) sharedLevel14
{
	NSAssert(instanceOfLevel14 != nil, @"Level14 instance not yet initialized!");
	return instanceOfLevel14;
}

-(id) initWithLevel14
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateFourteen:)];
        [self schedule:@selector(spawnBomb:) interval:((arc4random() % 3) + 5)];
        [self level14mods];
    }
    return self;
}

-(void) level14mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel14 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(251, 40, 40, 250)];
    [self addChild:colorLayer z:0];
        
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Tap the bombs to destroy them" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 14" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(50, (height - 30));
    [self addChild:levelDisplay];
    
    bombArrayRight = [[NSMutableArray alloc] init];
    bombArrayLeft = [[NSMutableArray alloc] init];
}


-(void) updateFourteen: (ccTime) dt
{
    [self moveBombs:bombArrayRight];
    [self moveBombs:bombArrayLeft];
    
    [self processRightBombCollisions:bombArrayRight];
    [self processLeftBombCollisions:bombArrayLeft];
    
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel1415 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver14 alloc] init]];
}

-(void) spawnBomb: (ccTime) dt
{
    CGFloat yStartRight = height + (arc4random() % 300);
    CGFloat yStartLeft = height + (arc4random() % 300);

    CCSprite * bombRight = [CCSprite spriteWithFile:@"bomb.png"];
    [self addChild:bombRight z:1];
    bombRight.position = ccp(width/2 + 150, yStartRight);
    [bombArrayRight addObject:bombRight];

    CCSprite * bombLeft = [CCSprite spriteWithFile:@"bomb.png"];
    [self addChild:bombLeft z:1];
    bombLeft.position = ccp(width/2 - 150, yStartLeft);
    [bombArrayLeft addObject:bombLeft];
}

-(void) moveBombs: (NSMutableArray *) array
{
    for (int i = 0; i < [array count]; i++)
    {
        [[array objectAtIndex:i] setPosition: ccp([(CCSprite*)[array objectAtIndex:i] position].x, [(CCSprite*)[array objectAtIndex:i] position].y - 1.5)];
    }
}

-(void) processRightBombCollisions: (NSMutableArray*) arrayEntry
{
    CGRect RightRect = CGRectMake(rightButton.position.x - width/2, height/2 + rightButton.position.y, rightButton.contentSize.width, rightButton.contentSize.height);
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        CGRect Box = CGRectMake(([[bombArrayRight objectAtIndex:i] boundingBox].origin.x - [[bombArrayRight objectAtIndex:i] contentSize].width/2 - 15), ([[bombArrayRight objectAtIndex:i] boundingBox].origin.y - 15), ([[bombArrayRight objectAtIndex:i] contentSize].width + 30), ([[bombArrayRight objectAtIndex:i]contentSize].height + 30));
        
        if ((CGRectContainsPoint(Box, pos)) )
        {
            ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
            [arrayEntry removeObjectAtIndex:i];
        }

        else if ([[arrayEntry objectAtIndex:i] boundingBox].origin.y < (RightRect.origin.y - RightRect.size.height/2 - 5) && [[arrayEntry objectAtIndex:i] boundingBox].origin.y > ((RightRect.origin.y - RightRect.size.height/2) - 7))
        {
//            missed--;
//            degrees = missed * 100;
//            [mainCreature setRotation: degrees];
////            [[CCDirector sharedDirector] replaceScene: [[GameOverLayer alloc] init]];
            [self switchScene];
            
            score = score - 10;
            CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
            CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:251 green:40 blue:40];
            CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
            [colorLayer runAction: sequence];
            
            [self removeChild:rightFallingObjects];
            [self removeChild:leftFallingObjects];

        }
    }
}


-(void) processLeftBombCollisions: (NSMutableArray*) arrayEntry
{
    
    CGRect LeftRect = CGRectMake(leftButton.position.x - width/2, height/2 + leftButton.position.y, leftButton.contentSize.width, leftButton.contentSize.height);
    
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        CGRect Box = CGRectMake(([[bombArrayLeft objectAtIndex:i] boundingBox].origin.x - [[bombArrayLeft objectAtIndex:i] contentSize].width/2 - 15), ([[bombArrayLeft objectAtIndex:i] boundingBox].origin.y - 15), ([[bombArrayLeft objectAtIndex:i] contentSize].width + 30), ([[bombArrayLeft objectAtIndex:i]contentSize].height + 30));
        
        if ((CGRectContainsPoint(Box, pos)) )
        {
            ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
            [arrayEntry removeObjectAtIndex:i];
        }

        else if ([[arrayEntry objectAtIndex:i] boundingBox].origin.y < (LeftRect.origin.y - LeftRect.size.height/2 - 5) && [[arrayEntry objectAtIndex:i] boundingBox].origin.y > ((LeftRect.origin.y - LeftRect.size.height/2) - 7))
        {
//            missed--;
//            degrees = missed * 100;
//            [mainCreature setRotation: degrees];
////            [[CCDirector sharedDirector] replaceScene: [[GameOverLayer alloc] init]];
            [self switchScene];
            score = score - 10;
            CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
            CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:251 green:40 blue:40];
            CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
            [colorLayer runAction: sequence];
            [self removeChild:rightFallingObjects];
            [self removeChild:leftFallingObjects];
        }
    }
}

-(void) processRightCollisions: (NSMutableArray*) arrayEntry
{
    CGRect RightRect = CGRectMake(rightButton.position.x - width/2, height/2 + rightButton.position.y, rightButton.contentSize.width, rightButton.contentSize.height);
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isRightClicked == TRUE)
        {
            if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (RightRect.origin.y  + RightRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (RightRect.origin.y - RightRect.size.height/2 - 5)))
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else
        {
            if ((([[arrayEntry objectAtIndex:i] boundingBox].origin.y) < (RightRect.origin.y - RightRect.size.height/2 - 5)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > (RightRect.origin.y - RightRect.size.height/2 - 6.7)))
            {
                score = score - 10;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:251 green:40 blue:40];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
            }
        }
    }
    
    isRightClicked = FALSE;
}


-(void) processLeftCollisions: (NSMutableArray*) arrayEntry
{
    
    CGRect LeftRect = CGRectMake(leftButton.position.x - width/2, height/2 + leftButton.position.y, leftButton.contentSize.width, leftButton.contentSize.height);
    
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isLeftClicked == TRUE)
        {
            if (([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (LeftRect.origin.y  + LeftRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (LeftRect.origin.y - LeftRect.size.height/2 - 5)))
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else
        {
            if ([[arrayEntry objectAtIndex:i] boundingBox].origin.y < (LeftRect.origin.y - LeftRect.size.height/2 - 5) && [[arrayEntry objectAtIndex:i] boundingBox].origin.y > ((LeftRect.origin.y - LeftRect.size.height/2) - 6.7))
            {
                score = score - 10;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:251 green:40 blue:40];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
                
            }
        }
    }
    
    isLeftClicked = FALSE;
}




@end
