//
//  NextLevel1617.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/5/13.
//
//

#import "NextLevel1617.h"
#import "Level17.h"

@implementation NextLevel1617
{
    CCMenu *startMenu;
    CCMenuItem *startButton;
}


-(id) initWithMods
{
    if (self = [super init])
    {
        [self modifications];
    }
    
    return self;
}

-(void) modifications
{
    // set background color: transparent color
    CCLayerColor* colorLayer = [CCLayerColor layerWithColor:ccc4(5, 95, 230, 200)];
    [self addChild:colorLayer z:0];
    
    // add a start button
    CCSprite *normalNextButton = [CCSprite spriteWithFile:@"next_pressed-hd.png"];
    CCSprite *selectedNextButton = [CCSprite spriteWithFile:@"next-hd.png"];
    startButton = [CCMenuItemSprite itemWithNormalSprite:normalNextButton selectedSprite:selectedNextButton block:^(id sender) {
        
        //called once Play button is pressed
        CCMoveTo *moveOffScreen = [CCMoveTo actionWithDuration:.6f position:ccp(self.position.x, self.contentSize.height * 2)];
        
        CCAction *movementCompleted = [CCCallBlock actionWithBlock:^{
            // cleanup
            //MainMenuLayer screen disappears
            self.visible = FALSE;
            //this layer calls its parent (gameplaylayer) and calls a specific method in it
            [[CCDirector sharedDirector] replaceScene: [[Level17 alloc] initWithLevel17]];
            
            //                [(GameLayer*)[self parent] startGame];
            //this layer gets trashed
            [self removeFromParent];
        }];
        
        CCSequence *menuHideMovement = [CCSequence actions:moveOffScreen, movementCompleted, nil];
        [self runAction:menuHideMovement];
        
    }];
    
    CCLabelTTF * levelCompleteLabel = [CCLabelTTF labelWithString:@"Continue" fontName:@"Marker Felt" fontSize:20.0f];
    CCMenuItem * levelComplete = [CCMenuItemLabel itemWithLabel:levelCompleteLabel];
    
    startMenu = [CCMenu menuWithItems: levelComplete, startButton, nil];
    
    startMenu.position = ccp(screenCenter.x, screenCenter.y - 85);
    [startMenu alignItemsVertically];
    [self addChild: startMenu z:1];
    
}

@end

