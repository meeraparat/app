//
//  Level22.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/7/13.
//
//

#import "Level22.h"
#import "NextLevel2223.h"
#import "GameOver22.h"

@implementation Level22


static Level22 * instanceOfLevel22;


//If another class wants to get a reference to this layer, they can by calling this method
+(Level22*) sharedLevel22
{
	NSAssert(instanceOfLevel22 != nil, @"Level22 instance not yet initialized!");
	return instanceOfLevel22;
}

-(id) initWithLevel22
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateTwentyTwo:)];
        [self schedule:@selector(spawnSpider:) interval:7];
        [self level22mods];
    }
    return self;
}

-(void) level22mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel22 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(252, 227, 85, 250)];
    [self addChild:colorLayer z:0];
    
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Tap on the spiders to kill them before it's too late!" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 22" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(50, (height - 30));
    [self addChild:levelDisplay];
    
    spiderArrayDown = [[NSMutableArray alloc] init];
    spiderArrayLeft = [[NSMutableArray alloc] init];
    spiderArrayUp = [[NSMutableArray alloc] init];
    spiderArrayRight = [[NSMutableArray alloc] init];
}


-(void) updateTwentyTwo: (ccTime) dt
{
    [self moveSpidersDown: spiderArrayDown];
    [self moveSpidersLeft:spiderArrayLeft];
//    [self moveSpidersUp:spiderArrayUp];
    [self moveSpidersRight:spiderArrayRight];
    
    [self processSpiderCollisions: spiderArrayDown];
    [self processSpiderCollisions:spiderArrayLeft];
    [self processSpiderCollisions:spiderArrayUp];
    [self processSpiderCollisions:spiderArrayRight];
    
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel2223 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver22 alloc] init]];
}

-(void) spawnSpider: (ccTime) dt
{
    spider1 = [CCSprite spriteWithFile:@"spider.png"];
    spider1.position = ccp(width/2 - 150, height);
    [self addChild:spider1 z:2 tag:1];
    [spiderArrayDown addObject:spider1];
    
//    spider2 = [CCSprite spriteWithFile:@"spider.png"];
//    spider2.position = ccp(width/2 + 150, height);
//    [self addChild:spider2 z:2 tag:2];
//    [spiderArrayDown addObject:spider2];

//    spider3 = [CCSprite spriteWithFile:@"spider.png"];
//    spider3.position = ccp(width + 15, (3*height)/4);
//    [self addChild:spider3 z:2 tag:3];
//    [spiderArrayLeft addObject:spider3];

    spider4 = [CCSprite spriteWithFile:@"spider.png"];
    spider4.position = ccp(width, height/2);
    [self addChild:spider4 z:2 tag:4];
    [spiderArrayLeft addObject:spider4];

//    spider5 = [CCSprite spriteWithFile:@"spider.png"];
//    spider5.position = ccp(width + 10, height/3);
//    [self addChild:spider5 z:2 tag:5];
//    [spiderArrayLeft addObject:spider5];

//    spider6 = [CCSprite spriteWithFile:@"spider.png"];
//    spider6.position = ccp(width/2 + 150, -5);
//    [self addChild:spider6 z:2 tag:6];
//    [spiderArrayUp addObject:spider6];

//    spider7 = [CCSprite spriteWithFile:@"spider.png"];
//    spider7.position = ccp(width/2 - 150, 0);
//    [self addChild:spider7 z:2 tag:7];
//    [spiderArrayUp addObject:spider7];

//    spider8 = [CCSprite spriteWithFile:@"spider.png"];
//    spider8.position = ccp(0, height/4);
//    [self addChild:spider8 z:2 tag:8];
//    [spiderArrayRight addObject:spider8];

    spider9 = [CCSprite spriteWithFile:@"spider.png"];
    spider9.position = ccp(-15, height/2);
    [self addChild:spider9 z:2 tag:9];
    [spiderArrayRight addObject:spider9];

//    spider10 = [CCSprite spriteWithFile:@"spider.png"];
//    spider10.position = ccp(-5, (4*height)/5);
//    [self addChild:spider10 z:2 tag:10];
//    [spiderArrayRight addObject:spider10];
//
}

-(void) moveSpidersDown: (NSMutableArray*) array
{
    for(int i = 0; i < [array count]; i++)
    {
        [[array objectAtIndex:i] setPosition: ccp([(CCSprite*)[array objectAtIndex:i] position].x, [(CCSprite*)[array objectAtIndex:i] position].y - 0.5)];
    }
}

-(void) moveSpidersLeft: (NSMutableArray*) array
{
    for(int i = 0; i < [array count]; i++)
    {
        [[array objectAtIndex:i] setPosition: ccp([(CCSprite*)[array objectAtIndex:i] position].x - 0.5, [(CCSprite*)[array objectAtIndex:i] position].y)];
    }
}

//-(void) moveSpidersUp: (NSMutableArray*) array
//{
//    for(int i = 0; i < [array count]; i++)
//    {
//        [[array objectAtIndex:i] setPosition: ccp([(CCSprite*)[array objectAtIndex:i] position].x, [(CCSprite*)[array objectAtIndex:i] position].y + 0.5)];
//    }
//}

-(void) moveSpidersRight: (NSMutableArray*) array
{
    for(int i = 0; i < [array count]; i++)
    {
        [[array objectAtIndex:i] setPosition: ccp([(CCSprite*)[array objectAtIndex:i] position].x + 0.5, [(CCSprite*)[array objectAtIndex:i] position].y)];
    }
}

-(void) processSpiderCollisions: (NSMutableArray *) array
{
    for (int i = 0; i < [array count]; i++)
    {
        if (([[array objectAtIndex:i] tag] == 1))
        {
            CGRect Box = CGRectMake(([[array objectAtIndex:i] boundingBox].origin.x - [[array objectAtIndex:i] contentSize].width/2 - 5), ([[array objectAtIndex:i] boundingBox].origin.y - 5), ([[array objectAtIndex:i] contentSize].width + 10), ([[array objectAtIndex:i]contentSize].height + 10));
            
            if ([[array objectAtIndex:i] position].y <= height/2)
            {
//                missed--;
//                degrees = missed * 100;
                ((CCSprite*)[array objectAtIndex:i]).visible = FALSE;
                [array removeObjectAtIndex:i];
//                [mainCreature setRotation: degrees];
//                [self deadDetector];
                [self switchScene];
            }
            else if ((CGRectContainsPoint(Box, pos)) )
            {
                ((CCSprite*)[array objectAtIndex:i]).visible = FALSE;
                [array removeObjectAtIndex:i];
            }
        }
        
        else if (([[array objectAtIndex:i] tag] == 4))
        {
            CGRect Box = CGRectMake(([[array objectAtIndex:i] boundingBox].origin.x - [[array objectAtIndex:i] contentSize].width/2 - 5), ([[array objectAtIndex:i] boundingBox].origin.y - 5), ([[array objectAtIndex:i] contentSize].width + 10), ([[array objectAtIndex:i]contentSize].height + 10));
            
            if ([[array objectAtIndex:i] position].x <= (width/2 + 150))
            {
                ((CCSprite*)[array objectAtIndex:i]).visible = FALSE;
                [array removeObjectAtIndex:i];
                [self switchScene];
            }
            else if ((CGRectContainsPoint(Box, pos)) )
            {
                ((CCSprite*)[array objectAtIndex:i]).visible = FALSE;
                [array removeObjectAtIndex:i];
            }
        }
        
//        if (([[array objectAtIndex:i] tag] == 7))
//        {
//            if ((CGRectContainsPoint(Box, pos)) )
//            {
//                ((CCSprite*)[array objectAtIndex:i]).visible = FALSE;
//                [array removeObjectAtIndex:i];
//            }
//
//            if ([[array objectAtIndex:i] position].y >= 70)
//            {
//                missed--;
//                degrees = missed * 100;
//                [mainCreature setRotation: degrees];
//                [self deadDetector];
//            }
//        }

        else if (([[array objectAtIndex:i] tag] == 9))
        {
            CGRect Box = CGRectMake(([[array objectAtIndex:i] boundingBox].origin.x - [[array objectAtIndex:i] contentSize].width/2 - 5), ([[array objectAtIndex:i] boundingBox].origin.y - 5), ([[array objectAtIndex:i] contentSize].width + 10), ([[array objectAtIndex:i]contentSize].height + 10));
            
            if ([[array objectAtIndex:i] position].x >= (width/2 - 150))
            {
                ((CCSprite*)[array objectAtIndex:i]).visible = FALSE;
                [array removeObjectAtIndex:i];
                [self switchScene];
            }
            else if ((CGRectContainsPoint(Box, pos)) )
            {
                ((CCSprite*)[array objectAtIndex:i]).visible = FALSE;
                [array removeObjectAtIndex:i];
            }
        }
    }
}


-(void) processRightCollisions: (NSMutableArray*) arrayEntry
{
    CGRect RightRect = CGRectMake(rightButton.position.x - width/2, height/2 + rightButton.position.y, rightButton.contentSize.width, rightButton.contentSize.height);
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isRightClicked == TRUE)
        {
            if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (RightRect.origin.y  + RightRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (RightRect.origin.y - RightRect.size.height/2 - 5)))
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else
        {
            if (([[arrayEntry objectAtIndex:i] boundingBox].origin.y < -5) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > -6.5))
            {
                score = score - 10;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:252 green:227 blue:85];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
            }
        }
    }
    
    isRightClicked = FALSE;
}


-(void) processLeftCollisions: (NSMutableArray*) arrayEntry
{
    
    CGRect LeftRect = CGRectMake(leftButton.position.x - width/2, height/2 + leftButton.position.y, leftButton.contentSize.width, leftButton.contentSize.height);
    
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isLeftClicked == TRUE)
        {
            if (([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (LeftRect.origin.y  + LeftRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (LeftRect.origin.y - LeftRect.size.height/2 - 5)))
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else
        {
            if (([[arrayEntry objectAtIndex:i] boundingBox].origin.y < -5) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > -6.5))
            {
                score = score - 10;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:252 green:227 blue:85];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
                
            }
        }
    }
    
    isLeftClicked = FALSE;
}



@end
