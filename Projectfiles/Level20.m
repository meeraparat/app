//
//  Level20.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/7/13.
//
//

#import "Level20.h"
#import "NextLevel2021.h"
#import "GameOver20.h"

@implementation Level20

static Level20 * instanceOfLevel20;


//If another class wants to get a reference to this layer, they can by calling this method
+(Level20*) sharedLevel20
{
	NSAssert(instanceOfLevel20 != nil, @"Level20 instance not yet initialized!");
	return instanceOfLevel20;
}

-(id) initWithLevel20
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateTwenty:)];
        [self level20mods];
    }
    return self;
}

-(void) level20mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel20 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(250, 15, 203, 250)];
    [self addChild:colorLayer z:0];
    
    rightButton.position = ccp(-width, -(height * 10));
    leftButton.position = ccp(-width, -(height * 10));

    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Only tap the Red and Blue ones" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 20" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(50, (height - 30));
    [self addChild:levelDisplay];
    
}


-(void) updateTwenty: (ccTime) dt
{
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel2021 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver20 alloc] init]];
}

-(void) processRightCollisions: (NSMutableArray *) arrayEntry
{
    for (int i = 0; i < [arrayEntry count]; i++)
    {
        if (([[arrayEntry objectAtIndex:i] tag] == 0) || ([[arrayEntry objectAtIndex:i] tag] == 1))
        {
            CGRect Box = CGRectMake(([[rightFallingObjects objectAtIndex:i] boundingBox].origin.x - [[rightFallingObjects objectAtIndex:i] contentSize].width/2 - 5), ([[rightFallingObjects objectAtIndex:i] boundingBox].origin.y - 5), ([[rightFallingObjects objectAtIndex:i] contentSize].width + 10), ([[rightFallingObjects objectAtIndex:i]contentSize].height + 10));
            
            
            if ((CGRectContainsPoint(Box, pos)) )
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else if ([[arrayEntry objectAtIndex:i] tag] == 2)
        {
            CGRect Box = CGRectMake(([[rightFallingObjects objectAtIndex:i] boundingBox].origin.x - [[rightFallingObjects objectAtIndex:i] contentSize].width/2), ([[rightFallingObjects objectAtIndex:i] boundingBox].origin.y), ([[rightFallingObjects objectAtIndex:i] contentSize].width), ([[rightFallingObjects objectAtIndex:i]contentSize].height));
            
            if (CGRectContainsPoint(Box, pos))
            {
                score = score - 10;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:250 green:15 blue:203];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
            }
        }
    }
}


-(void) processLeftCollisions: (NSMutableArray *) arrayEntry
{
    for (int i = 0; i < [arrayEntry count]; i++)
    {
        if (([[arrayEntry objectAtIndex:i] tag] == 0) || ([[arrayEntry objectAtIndex:i] tag] == 1))
        {
            CGRect Box = CGRectMake(([[leftFallingObjects objectAtIndex:i] boundingBox].origin.x - [[leftFallingObjects objectAtIndex:i] contentSize].width/2 - 5), ([[leftFallingObjects objectAtIndex:i] boundingBox].origin.y - 5), ([[leftFallingObjects objectAtIndex:i] contentSize].width + 10), ([[leftFallingObjects objectAtIndex:i]contentSize].height + 10));
            
            
            if ((CGRectContainsPoint(Box, pos)) )
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else if ([[arrayEntry objectAtIndex:i] tag] == 2)
        {
            CGRect Box = CGRectMake(([[leftFallingObjects objectAtIndex:i] boundingBox].origin.x - [[leftFallingObjects objectAtIndex:i] contentSize].width/2), ([[leftFallingObjects objectAtIndex:i] boundingBox].origin.y), ([[leftFallingObjects objectAtIndex:i] contentSize].width), ([[leftFallingObjects objectAtIndex:i]contentSize].height));
            
            if ((CGRectContainsPoint(Box, pos)))
            {
                score = score - 10;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:250 green:15 blue:203];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];            }
        }
    }
}


@end
