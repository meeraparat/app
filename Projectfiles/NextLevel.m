//
//  NextLevel.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 7/19/13.
//
//

#import "NextLevel.h"
#import "Level2.h"
#import "GameLayer.h"

@implementation NextLevel

{
    CCNode *goodJobTitle;
    CCMenu *startMenu;
    CCMenuItem *startButton;
    CGPoint goodJobTitleTargetPoint;
}


-(id) init
{
	if (self = [super init])
    {
        // set background color: transparent color
        CCLayerColor* colorLayer = [CCLayerColor layerWithColor:ccc4(50, 220, 183, 200)];
        [self addChild:colorLayer z:0];
        
//        //setup the start menu title
//        if (FALSE) {
//            // OPTION 1: Title as Text
//            CCLabelTTF *tempStartTitleLabel = [CCLabelTTF labelWithString:@"Good Job!"
//                                                                 fontName:@"Arial"
//                                                                 fontSize:20];
//            tempStartTitleLabel.color = ccc3(255, 255, 255);
//            StartTitleLabel = tempStartTitleLabel;
//        } else {
//            // OPTION 2: Title as Sprite
//        CCSprite *goodJobLabel = [CCSprite spriteWithFile:@"title.png"];
        CCLabelTTF * goodJobLabel = [CCLabelTTF labelWithString:@"Good Job!" fontName:@"Marker Felt" fontSize:40.0f];
        goodJobTitle = goodJobLabel;
//        }
        
        screenCenter = [CCDirector sharedDirector].screenCenter;
        screenSize = [CCDirector sharedDirector].screenSize;
        
        // place the goodJobTitle off-screen, later we will animate it on screen
        goodJobTitle.position = ccp (screenCenter.x, screenSize.height + 100);
        
        // this will be the point, we will animate the title to
        goodJobTitleTargetPoint = ccp(screenCenter.x, screenSize.height - 60);
        
		[self addChild:goodJobTitle z:1];
        
        // add a start button
        CCSprite *normalNextButton = [CCSprite spriteWithFile:@"next_pressed-hd.png"];
        CCSprite *selectedNextButton = [CCSprite spriteWithFile:@"next-hd.png"];
        startButton = [CCMenuItemSprite itemWithNormalSprite:normalNextButton selectedSprite:selectedNextButton block:^(id sender) {
            
            //called once Play button is pressed
            CCMoveTo *moveOffScreen = [CCMoveTo actionWithDuration:.6f position:ccp(self.position.x, self.contentSize.height * 2)];
            
            CCAction *movementCompleted = [CCCallBlock actionWithBlock:^{
                // cleanup
                //MainMenuLayer screen disappears
                self.visible = FALSE;
                //this layer calls its parent (gameplaylayer) and calls a specific method in it
                [[CCDirector sharedDirector] replaceScene: [[Level2 alloc] initWithLevel2]];

//                [(GameLayer*)[self parent] startGame];
                //this layer gets trashed
                [self removeFromParent];
            }];
            
            CCSequence *menuHideMovement = [CCSequence actions:moveOffScreen, movementCompleted, nil];
            [self runAction:menuHideMovement];
            
        }];
        
        CCLabelTTF * levelCompleteLabel = [CCLabelTTF labelWithString:@"Continue" fontName:@"Marker Felt" fontSize:20.0f];
        CCMenuItem * levelComplete = [CCMenuItemLabel itemWithLabel:levelCompleteLabel];
        
        startMenu = [CCMenu menuWithItems: levelComplete, startButton, nil];
        startMenu.position = ccp(screenCenter.x, screenCenter.y - 85);
        [startMenu alignItemsVertically];
        [self addChild: startMenu z:1];
        
        CCLabelTTF * earnedStarLabel = [CCLabelTTF labelWithString:@"You earned" fontName:@"Marker Felt" fontSize:20.0f];
        CCMenuItem * earnedStarText = [CCMenuItemLabel itemWithLabel:earnedStarLabel];
        
        CCSprite * star = [CCSprite spriteWithFile:@"star.png"];
        CCSprite * selectedStar = [CCSprite spriteWithFile:@"star.png"];
        star = [CCMenuItemSprite itemWithNormalSprite:star selectedSprite:selectedStar];
        
        CCMenu * starMenu = [CCMenu menuWithItems:earnedStarText, star, nil];
        starMenu.position = ccp(screenCenter.x, screenCenter.y + 25);
        [starMenu alignItemsVertically];
        [self addChild:starMenu z:1];
        
    
    }
    
	return self;
}

#pragma mark - Scene Lifecyle

/**
 This method is called when the scene becomes visible. You should add any code, that shall be executed once
 the scene is visible, to this method.
 */
-(void)onEnterTransitionDidFinish
{
    [super onEnterTransitionDidFinish];
    
    // animate the title on to screen
    CCMoveTo *move = [CCMoveTo actionWithDuration:1.f position:goodJobTitleTargetPoint];
    id easeMove = [CCEaseBackInOut actionWithAction:move];
    [goodJobTitle runAction: easeMove];
    
}


@end
