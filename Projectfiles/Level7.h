//
//  Level7.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 1/6/14.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"
#import "Level4.h"

@interface Level7 : Level4

-(id) initWithLevel7;
+(Level7*) sharedLevel7;

@end
