//
//  Level18.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/5/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"

@interface Level18 : GameLayer

-(id) initWithLevel18;
+(Level18*) sharedLevel18;

@end
