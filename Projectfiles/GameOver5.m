//
//  GameOver5.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver5.h"
#import "Level5.h"

@implementation GameOver5

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level5 alloc] initWithLevel5]];
}

@end
