//
//  Level28.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/8/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"
#import "Level23.h"

@interface Level28 : Level23

-(id) initWithLevel28;
+(Level28*) sharedLevel28;

@end
