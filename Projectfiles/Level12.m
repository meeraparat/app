//
//  Level12.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/2/13.
//
//

#import "Level12.h"
#import "NextLevel1213.h"
#import "GameOver12.h"

@implementation Level12

static Level12 * instanceOfLevel12;



//If another class wants to get a reference to this layer, they can by calling this method
+(Level12*) sharedLevel12
{
	NSAssert(instanceOfLevel12 != nil, @"Level12 instance not yet initialized!");
	return instanceOfLevel12;
}

-(id) initWithLevel12
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateTwelve:)];
//        [self schedule:@selector(spawnBabyRight:) interval:((arc4random() % 3) + 3)];
//        [self schedule:@selector(spawnBabyLeft:) interval:((arc4random() % 3) + 3)];
        [self level12mods];
    }
    return self;
}

-(void) level12mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel12 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(235, 101, 533, 250)];
    [self addChild:colorLayer z:0];
    
    rightButton.position = ccp(-width, -(height * 10));
    leftButton.position = ccp(-width, -(height * 10));
    
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Faster!" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 12" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(50, (height - 30));
    [self addChild:levelDisplay];
    
//    alligatorRight = [CCSprite spriteWithFile:@"alligator.png"];
//    alligatorRight.position = ccp(width/2 + 150, height/2 + 15);
//    [self addChild:alligatorRight z:2];
//    
//    alligatorLeft = [CCSprite spriteWithFile:@"alligator.png"];
//    alligatorLeft.position = ccp(width/2 - 150, height/2 + 15);
//    [self addChild:alligatorLeft z:2];
//    
//    babyRight = [CCSprite spriteWithFile:@"baby.png"];
//    babyRight.position = ccp(width/2 + 150, height);
//    [self addChild:babyRight z:2];
//    [rightFallingObjects addObject:babyRight];
//    
//    babyLeft = [CCSprite spriteWithFile:@"baby.png"];
//    babyLeft.position = ccp(width/2 - 150, height);
//    [self addChild:babyLeft z:2];
//    [leftFallingObjects addObject:babyLeft];


    
//    babyArrayRight = [[NSMutableArray alloc] init];
//    babyArrayLeft = [[NSMutableArray alloc] init];
    
//    [self babyCollisionDetection:babyArrayRight];
//    [self babyCollisionDetection:babyArrayLeft];
}

-(void) updateTwelve: (ccTime) dt
{
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel1213 alloc] initWithMods]];
    }
    
//    [self moveBabies:babyArrayRight];
//    [self moveBabies:babyArrayLeft];
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver12 alloc] init]];
}

-(void) moveFallingObjects: (NSMutableArray*) fallingObjectArray
{
    for(int i = 0; i < [fallingObjectArray count]; i++)
    {
        [[fallingObjectArray objectAtIndex:i] setPosition: ccp([(CCSprite*)[fallingObjectArray objectAtIndex:i] position].x, [(CCSprite*)[fallingObjectArray objectAtIndex:i] position].y - 2.5)];
    }
}


//-(void) moveBabies: (NSMutableArray*) array
//{
//    for (int i = 0; i < [array count]; i++)
//    {
//        [[array objectAtIndex:i] setPosition:ccp([[array objectAtIndex:i] position].x, ([[array objectAtIndex:i] position].y - 2.5))];
//    }
//}
//-(void) spawnBabyRight: (ccTime) dt
//{
//    babyRight = [CCSprite spriteWithFile:@"baby.png"];
//    babyRight.position = ccp(width/2 + 150, height);
//    [self addChild:babyRight z:2];
//    [rightFallingObjects addObject:babyRight];
//}
//
//-(void) spawnBabyLeft: (ccTime) dt
//{
//    babyLeft = [CCSprite spriteWithFile:@"baby.png"];
//    babyLeft.position = ccp(width/2 - 150, height);
//    [self addChild:babyLeft z:2];
//    [leftFallingObjects addObject:babyLeft];
//}

//-(void) babyCollisionDetection: (NSMutableArray*) array
//{
//    for (int i = 0; i < [array count]; i ++)
//    {
//        CGRect babyBox = CGRectMake(([[array objectAtIndex:i] boundingBox].origin.x - [[array objectAtIndex:i] contentSize].width/2), [[array objectAtIndex:i] boundingBox].origin.y, [[array objectAtIndex:i] contentSize].width, [[array objectAtIndex:i] contentSize].height);
//        
//        CGRect alligatorBoxLeft = CGRectMake(alligatorLeft.position.x, alligatorLeft.position.y, alligatorLeft.size.width, alligatorLeft.size.height);
//        CGRect alligatorBoxRight = CGRectMake(alligatorRight.position.x, alligatorRight.position.y, alligatorRight.size.width, alligatorRight.size.height);
//        
//        if ((CGRectIntersectsRect(babyBox, alligatorBoxLeft)) || (CGRectIntersectsRect(babyBox, alligatorBoxRight)))
//        {
//            NSLog(@"true");
//            if ((CGRectContainsPoint(alligatorBoxLeft, pos)) || (CGRectContainsPoint(alligatorBoxRight, pos)))
//            {
//                ((CCSprite*)[array objectAtIndex:i]).visible = FALSE;
//                [array removeObjectAtIndex:i];
//            }
//            else
//            {
//                [self deadDetector];
//            }
//        }
//        
//    }
//}


@end
