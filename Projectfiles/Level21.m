//
//  Level21.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/7/13.
//
//

#import "Level21.h"
#import "NextLevel2122.h"
#import "GameOver21.h"

@implementation Level21

static Level21 * instanceOfLevel21;


//If another class wants to get a reference to this layer, they can by calling this method
+(Level21*) sharedLevel21
{
	NSAssert(instanceOfLevel21 != nil, @"Level21 instance not yet initialized!");
	return instanceOfLevel21;
}

-(id) initWithLevel21
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateTwentyOne:)];
        [self level21mods];
    }
    return self;
}

-(void) level21mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel21 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(253, 180, 233, 250)];
    [self addChild:colorLayer z:0];
    
    rightButton.position = ccp(-width, -(height * 10));
    leftButton.position = ccp(-width, -(height * 10));
    
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Freeby #2 - click anywhere" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 21" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(50, (height - 30));
    [self addChild:levelDisplay];
    
}


-(void) updateTwentyOne: (ccTime) dt
{
    if (touchCounter == 7)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel2122 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver21 alloc] init]];
}


@end
