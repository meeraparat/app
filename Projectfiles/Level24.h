//
//  Level24.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/8/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"
#import "Level18.h"

@interface Level24 : Level18

-(id) initWithLevel24;
+(Level24*) sharedLevel24;

@end
