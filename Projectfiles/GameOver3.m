//
//  GameOver3.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver3.h"
#import "Level3.h"

@implementation GameOver3

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level3 alloc] initWithLevel3]];
}

@end
