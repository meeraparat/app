//
//  Level19.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/5/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"

@interface Level19 : GameLayer

-(id) initWithLevel19;
+(Level19*) sharedLevel19;

@end
