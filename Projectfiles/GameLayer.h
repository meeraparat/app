/*
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen Itterheim, Andreas Loew 
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
 */

//  Updated by Andreas Loew on 20.06.11:
//  * retina display
//  * framerate independency
//  * using TexturePacker http://www.texturepacker.com

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "FallingObjects.h"
#import "CCLayer.h"
//#import "GameLevel2.h"


//This assigns an integer value to an arbitrarily long list, where each has a value 1 greater than the last. So here TouchAvailableLabelTag = 3 and TouchEndedLabelTag = 4
typedef enum
{
	TouchStartedLabelTag = 2,
	TouchAvailableLabelTag = 3,
	TouchEndedLabelTag = 4,
	
} LabelTags;

@interface GameLayer : CCLayer
{
    KKInput *input;
    
    BOOL isRightClicked;
    BOOL isLeftClicked;

    CCMenu * starDisplay;
//    CCSprite * star;
//    CCSprite * selectedStar;
        
//    NSNumber *coinCounter;
    NSNumber *savedCoinCounter;
    
    int numberComplete;
    
    CGPoint pos;
    int touchCounter;
    CCLabelTTF * levelDisplay;
    
    BOOL touched;
    
    CCMenuItemImage * rightButton;
    CCMenuItemImage * leftButton;
    CGFloat width;
    CGFloat height;
    
    CGFloat yStartRedRight;
    CGFloat yStartBlueRight;
    CGFloat yStartGreenRight;
    CGFloat yStartRedLeft;
    CGFloat yStartBlueLeft;
    CGFloat yStartGreenLeft;
    
    NSMutableArray *rightFallingObjects;
    NSMutableArray *leftFallingObjects;
    CCSprite * mainCreature;
    float missed;
    float degrees;
    CCLabelTTF * musicNote;
    CCLabelTTF * iceCube;
    BOOL noteType;
    BOOL iceCubeType;
    CCLabelTTF * scoreLabel;
    int score;
    
    CGPoint screenCenter;
    CGSize screenSize;
    
    CCLabelTTF * hint;
    CCLabelTTF * rulesHelpLabel;
    CCMenuItemImage * arrow;
    CCMenu * appearingHintMenu;
    
    CCLayerColor* colorLayer;
    ccColor3B * baseColor;
}

-(void) deadDetector;
+(CGRect) screenRect;
+(GameLayer*) sharedGameLayer;
-(void) updateScore:(int) newScore;


@end
