//
//  Level15.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/4/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"

@interface Level15 : GameLayer
{
    BOOL redClicked;
}
-(id) initWithLevel15;
+(Level15*) sharedLevel15;

@end
