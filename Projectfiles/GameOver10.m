//
//  GameOver10.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver10.h"
#import "Level10.h"

@implementation GameOver10

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level10 alloc] initWithLevel10]];
}

@end
