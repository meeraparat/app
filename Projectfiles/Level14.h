//
//  Level14.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/4/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"

@interface Level14 : GameLayer
{
    NSMutableArray * bombArrayRight;
    NSMutableArray * bombArrayLeft;
}

-(id) initWithLevel14;
+(Level14*) sharedLevel14;

@end
