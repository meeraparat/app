//
//  Level21.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/7/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"

@interface Level21 : GameLayer

-(id) initWithLevel21;
+(Level21*) sharedLevel21;

@end
