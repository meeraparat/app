//
//  GameOver11.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver11.h"
#import "Level11.h"

@implementation GameOver11

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level11 alloc] initWithLevel11]];
}

@end
