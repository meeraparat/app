//
//  Level26.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/8/13.
//
//

#import "Level26.h"
#import "NextLevel2627.h"
#import "GameOver26.h"

@implementation Level26


static Level26 * instanceOfLevel26;


//If another class wants to get a reference to this layer, they can by calling this method
+(Level26*) sharedLevel26
{
	NSAssert(instanceOfLevel26 != nil, @"Level26 instance not yet initialized!");
	return instanceOfLevel26;
}

-(id) initWithLevel26
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateTwentySix:)];
        [self level26mods];
    }
    return self;
}

-(void) level26mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel26 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(251, 35, 49, 250)];
    [self addChild:colorLayer z:0];
        
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Tap the buttons to send bullets upward" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 26" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(50, (height - 30));
    [self addChild:levelDisplay];
    
    rightBulletArray = [[NSMutableArray alloc] init];
    leftBulletArray = [[NSMutableArray alloc] init];
}


-(void) updateTwentySix: (ccTime) dt
{
    [self moveBullets:rightBulletArray];
    [self moveBullets:leftBulletArray];

    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel2627 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver26 alloc] init]];
}

-(void) moveBullets: (NSMutableArray*) array
{
    for (int i = 0; i < [array count]; i++)
    {
        [[array objectAtIndex:i] setPosition: ccp([(CCSprite*)[array objectAtIndex:i] position].x, [(CCSprite*)[array objectAtIndex:i] position].y + 2.2)];
        if ([[array objectAtIndex:i] position].y == height)
        {
            ((CCSprite*)[array objectAtIndex:i]).visible = FALSE;
            [array removeObjectAtIndex:i];
        }
    }
}

-(void) rightClicked: (CCMenuItemImage *) rightbutton
{
    isRightClicked = TRUE;
    
    CCSprite * bullet = [CCSprite spriteWithFile:@"black_dot.gif"];
    bullet.position = ccp(width/2 + 150, height/2 - 75);
    [self addChild:bullet z:2];
    [rightBulletArray addObject:bullet];
    
}

-(void) leftClicked: (CCMenuItemImage *) leftButton
{
    isLeftClicked = TRUE;
    
    CCSprite * bullet = [CCSprite spriteWithFile:@"black_dot.gif"];
    bullet.position = ccp(width/2 - 150, height/2 - 75);
    [self addChild:bullet z:2];
    [leftBulletArray addObject:bullet];
}

-(void) processRightCollisions: (NSMutableArray*) arrayEntry
{
    CGRect RightRect = CGRectMake(rightButton.position.x - width/2, height/2 + rightButton.position.y, rightButton.contentSize.width, rightButton.contentSize.height);
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
//        if (isRightClicked == TRUE)
//        {
        if ([[arrayEntry objectAtIndex:i] position].y > RightRect.origin.y)
        {
            for(int i = 0; i < [rightBulletArray count]; i++)
            {
                if([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= [[rightBulletArray objectAtIndex:i] position].y)
                {
                    ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                    [arrayEntry removeObjectAtIndex:i];
                    ((CCSprite*) [rightBulletArray objectAtIndex:i]).visible = false;
                    [rightBulletArray removeObjectAtIndex:i];
                    score = score + 10;
                }
            }
        }
        isRightClicked = false;
//        }
//        else
//        {
            if ((([[arrayEntry objectAtIndex:i] boundingBox].origin.y) < (RightRect.origin.y - RightRect.size.height/2 - 5)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > (RightRect.origin.y - RightRect.size.height/2 - 7)))
            {
                score = score - 10;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:251 green:35 blue:49];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
                
            }
//        }
    }
    
//    isRightClicked = FALSE;
}


-(void) processLeftCollisions: (NSMutableArray*) arrayEntry
{
    CGRect LeftRect = CGRectMake(leftButton.position.x - width/2, height/2 + leftButton.position.y, leftButton.contentSize.width, leftButton.contentSize.height);
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
//        if (isLeftClicked == TRUE)
//        {
        if ([[arrayEntry objectAtIndex:i] position].y > LeftRect.origin.y)
        {
            for(int i = 0; i < [leftBulletArray count]; i++)
            {
                if([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= [[leftBulletArray objectAtIndex:i] position].y)
                {
                    ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                    [arrayEntry removeObjectAtIndex:i];
                    ((CCSprite*) [leftBulletArray objectAtIndex:i]).visible = false;
                    [leftBulletArray removeObjectAtIndex:i];
                    score = score + 10;
                }
            }
        }
    isLeftClicked = false;
//        }
//        else
//        {
            if ([[arrayEntry objectAtIndex:i] boundingBox].origin.y < (LeftRect.origin.y - LeftRect.size.height/2 - 5) && [[arrayEntry objectAtIndex:i] boundingBox].origin.y > ((LeftRect.origin.y - LeftRect.size.height/2) - 7))
            {
                score = score - 10;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:251 green:35 blue:49];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];            }
//        }
    }
    
//    isLeftClicked = FALSE;
}


@end
