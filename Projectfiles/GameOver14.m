//
//  GameOver14.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver14.h"
#import "Level14.h"

@implementation GameOver14

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level14 alloc] initWithLevel14]];
}

@end
