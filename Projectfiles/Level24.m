//
//  Level24.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/8/13.
//
//

#import "Level24.h"
#import "NextLevel2425.h"
#import "GameOver24.h"

@implementation Level24


static Level24 * instanceOfLevel24;


//If another class wants to get a reference to this layer, they can by calling this method
+(Level24*) sharedLevel24
{
	NSAssert(instanceOfLevel24 != nil, @"Level24 instance not yet initialized!");
	return instanceOfLevel24;
}

-(id) initWithLevel24
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateTwentyFour:)];
        [self level24mods];
    }
    return self;
}

-(void) level24mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel24 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(120, 2, 55, 250)];
    [self addChild:colorLayer z:0];
    
    rightButton.position = ccp(150, -140);
    leftButton.position = ccp(-150, -140);
    
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"They may appear anywhere" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 24" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(50, (height - 30));
    [self addChild:levelDisplay];
    
}


-(void) updateTwentyFour: (ccTime) dt
{
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel2425 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver24 alloc] init]];
}


-(void) moveFallingObjects: (NSMutableArray*) fallingObjectArray
{
    for(int i = 0; i < [fallingObjectArray count]; i++)
    {
        [[fallingObjectArray objectAtIndex:i] setPosition: ccp([(CCSprite*)[fallingObjectArray objectAtIndex:i] position].x, [(CCSprite*)[fallingObjectArray objectAtIndex:i] position].y - 2.5)];
    }
}

-(void) processRightCollisions: (NSMutableArray*) arrayEntry
{
    CGRect RightRect = CGRectMake(rightButton.position.x - width/2, height/2 + rightButton.position.y, rightButton.contentSize.width, rightButton.contentSize.height);
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isRightClicked == TRUE)
        {
            if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (RightRect.origin.y  + RightRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (RightRect.origin.y - RightRect.size.height/2 - 5)))
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else
        {
            if ((([[arrayEntry objectAtIndex:i] boundingBox].origin.y) < (RightRect.origin.y - RightRect.size.height/2 - 5)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > (RightRect.origin.y - RightRect.size.height/2 - 7)))
            {
                //                missed++;
                //                degrees = missed * 5;
                //                [mainCreature setRotation: degrees];
                score = score - 10;
                //                [self deadDetector];
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:120 green:2 blue:55];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
            }
        }
    }
    
    isRightClicked = FALSE;
}


-(void) processLeftCollisions: (NSMutableArray*) arrayEntry
{
    
    CGRect LeftRect = CGRectMake(leftButton.position.x - width/2, height/2 + leftButton.position.y, leftButton.contentSize.width, leftButton.contentSize.height);
    
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isLeftClicked == TRUE)
        {
            if (([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (LeftRect.origin.y  + LeftRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (LeftRect.origin.y - LeftRect.size.height/2 - 5)))
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else
        {
            if ([[arrayEntry objectAtIndex:i] boundingBox].origin.y < (LeftRect.origin.y - LeftRect.size.height/2 - 5) && [[arrayEntry objectAtIndex:i] boundingBox].origin.y > ((LeftRect.origin.y - LeftRect.size.height/2) - 7))
            {
                //                missed--;
                //                degrees = missed * 5;
                //                [mainCreature setRotation: degrees];
                score = score - 10;
                //                [self deadDetector];
                //                CCLayerColor * baseColor = colorLayer;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:120 green:2 blue:55];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
                
            }
        }
    }
    
    isLeftClicked = FALSE;
}


@end
