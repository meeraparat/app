//
//  GameOver23.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver23.h"
#import "Level23.h"

@implementation GameOver23

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level23 alloc] initWithLevel23]];
}

@end
