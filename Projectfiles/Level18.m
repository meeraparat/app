//
//  Level18.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/5/13.
//
//

#import "Level18.h"
#import "NextLevel1819.h"
#import "GameOver18.h"

@implementation Level18


static Level18 * instanceOfLevel18;


//If another class wants to get a reference to this layer, they can by calling this method
+(Level18*) sharedLevel18
{
	NSAssert(instanceOfLevel18 != nil, @"Level18 instance not yet initialized!");
	return instanceOfLevel18;
}

-(id) initWithLevel18
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateEighteen:)];
        [self level18mods];
    }
    return self;
}

-(void) level18mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel18 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(252, 137, 105, 250)];
    [self addChild:colorLayer z:0];
    
    rightButton.position = ccp(150, -130);
    leftButton.position = ccp(-150, -130);
    
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"They may appear at anytime" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 18" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(50, (height - 30));
    [self addChild:levelDisplay];
    
}


-(void) updateEighteen: (ccTime) dt
{
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel1819 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver18 alloc] init]];
}

-(void) spawnFallingObject: (ccTime) dt
{
    yStartRedRight = 100 + (arc4random() % 400);
    yStartBlueRight = 100 + (arc4random() % 400);
    yStartGreenRight = 100 + (arc4random() % 400);
    
    yStartRedLeft = 100 + (arc4random() % 400);
    yStartBlueLeft = 100 + (arc4random() % 400);
    yStartGreenLeft = 100 + (arc4random() % 400);
    
    FallingObjects * redRight = [FallingObjects getType:0];
    FallingObjects * blueRight = [FallingObjects getType:1];
    FallingObjects * greenRight = [FallingObjects  getType:2];
    
    FallingObjects * redLeft = [FallingObjects getType:0];
    FallingObjects * blueLeft = [FallingObjects getType:1];
    FallingObjects * greenLeft = [FallingObjects  getType:2];
    
    
    
    [self addChild:redRight z:1 tag:0];
    redRight.position = ccp(width/2 + 150, yStartRedRight);
    
    [self addChild:blueRight z:1 tag:1];
    blueRight.position = ccp(width/2 + 150, yStartBlueRight);
    
    [self addChild:greenRight z:1 tag:2];
    greenRight.position = ccp(width/2 + 150, yStartGreenRight);
    
    ////////////////////
    
    [self addChild:redLeft z:1 tag:0];
    redLeft.position = ccp(width/2 - 150, yStartRedLeft);
    
    [self addChild:blueLeft z:1 tag:1];
    blueLeft.position = ccp(width/2 - 150, yStartBlueLeft);
    
    [self addChild:greenLeft z:1 tag:2];
    greenLeft.position = ccp(width/2 - 150, yStartGreenLeft);
    
    [rightFallingObjects addObject:redRight];
    [rightFallingObjects addObject:blueRight];
    [rightFallingObjects addObject:greenRight];
    
    [leftFallingObjects addObject:redLeft];
    [leftFallingObjects addObject:blueLeft];
    [leftFallingObjects addObject:greenLeft];
    
}

-(void) processRightCollisions: (NSMutableArray*) arrayEntry
{
    CGRect RightRect = CGRectMake(rightButton.position.x - width/2, height/2 + rightButton.position.y, rightButton.contentSize.width, rightButton.contentSize.height);
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isRightClicked == TRUE)
        {
            if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (RightRect.origin.y  + RightRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (RightRect.origin.y - RightRect.size.height/2 - 5)))
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else
        {
            if ((([[arrayEntry objectAtIndex:i] boundingBox].origin.y) < (RightRect.origin.y - RightRect.size.height/2 - 5)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > (RightRect.origin.y - RightRect.size.height/2 - 7)))
            {
                //                missed++;
                //                degrees = missed * 5;
                //                [mainCreature setRotation: degrees];
                score = score - 10;
                //                [self deadDetector];
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:252 green:137 blue:105];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
            }
        }
    }
    
    isRightClicked = FALSE;
}


-(void) processLeftCollisions: (NSMutableArray*) arrayEntry
{
    
    CGRect LeftRect = CGRectMake(leftButton.position.x - width/2, height/2 + leftButton.position.y, leftButton.contentSize.width, leftButton.contentSize.height);
    
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isLeftClicked == TRUE)
        {
            if (([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (LeftRect.origin.y  + LeftRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (LeftRect.origin.y - LeftRect.size.height/2 - 5)))
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else
        {
            if ([[arrayEntry objectAtIndex:i] boundingBox].origin.y < (LeftRect.origin.y - LeftRect.size.height/2 - 5) && [[arrayEntry objectAtIndex:i] boundingBox].origin.y > ((LeftRect.origin.y - LeftRect.size.height/2) - 7))
            {
                //                missed--;
                //                degrees = missed * 5;
                //                [mainCreature setRotation: degrees];
                score = score - 10;
                //                [self deadDetector];
                //                CCLayerColor * baseColor = colorLayer;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:252 green:137 blue:105];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
                
            }
        }
    }
    
    isLeftClicked = FALSE;
}





@end
