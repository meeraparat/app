//
//  Tutorial.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 9/3/13.
//
//

#import "Tutorial.h"
#import "GenericMenuLayer.h"

@implementation Tutorial
{
    CCNode *tutorialTitle;
    CCMenu * tutorialMenu;
    CCMenuItem *backButton;
    CGPoint tutorialTitleTargetPoint;

}


-(id) init
{
	if ((self = [super init]))
	{
        CCLayerColor* colorLayer = [CCLayerColor layerWithColor:ccc4(250, 250, 250, 250)];
        [self addChild:colorLayer z:0];
                
        
        CCLabelTTF * tutorialLabel = [CCLabelTTF labelWithString:@"Click on the button as the objects fall down on top of them. \n There are hints at the bottom of each screen \n that tell you the rules for each level. \n \n Earn 1 star for every level completed. \n \n Spend 2 stars to skip a level." fontName:@"Marker Felt" fontSize:15.0f];
        tutorialLabel.color = ccc3(0, 0, 0);
        tutorialTitle = tutorialLabel;
        //        }
        
        screenCenter = [CCDirector sharedDirector].screenCenter;
        screenSize = [CCDirector sharedDirector].screenSize;
        
        // place the goodJobTitle off- screen, later we will animate it on screen
        tutorialTitle.position = ccp (screenCenter.x, screenSize.height + 100);
        
        // this will be the point, we will animate the title to
        tutorialTitleTargetPoint = ccp(screenCenter.x, [CCDirector sharedDirector].screenCenter.y);
        
		[self addChild:tutorialTitle z:1];

                
        // add a start button
        CCSprite *normalBackButton = [CCSprite spriteWithFile:@"next_pressed-hd.png"];
        CCSprite *selectedBackButton = [CCSprite spriteWithFile:@"next-hd.png"];
        backButton = [CCMenuItemSprite itemWithNormalSprite:normalBackButton selectedSprite:selectedBackButton block:^(id sender) {
            
            //called once Play button is pressed
            CCMoveTo *moveOffScreen = [CCMoveTo actionWithDuration:.6f position:ccp(self.position.x, self.contentSize.height * 2)];
            
            CCAction *movementCompleted = [CCCallBlock actionWithBlock:^{
                // cleanup
                //MainMenuLayer screen disappears
                self.visible = FALSE;
                //this layer calls its parent (gameplaylayer) and calls a specific method in it
                [[CCDirector sharedDirector] replaceScene: [[GenericMenuLayer alloc] init]];
                
                //                [(GameLayer*)[self parent] startGame];
                //this layer gets trashed
                [self removeFromParent];
            }];
            
            CCSequence *menuHideMovement = [CCSequence actions:moveOffScreen, movementCompleted, nil];
            [self runAction:menuHideMovement];
            
        }];
                
        tutorialMenu = [CCMenu menuWithItems: backButton, nil];
        tutorialMenu.position = ccp([CCDirector sharedDirector].screenCenter.x, [CCDirector sharedDirector].screenCenter.y);
        tutorialMenu.position = ccp(50, 275);
        [self addChild: tutorialMenu z:1];
        
    }
    
	return self;
}

#pragma mark - Scene Lifecyle

/**
 This method is called when the scene becomes visible. You should add any code, that shall be executed once
 the scene is visible, to this method.
 */
-(void)onEnterTransitionDidFinish
{
    [super onEnterTransitionDidFinish];
    
    // animate the title on to screen
    CCMoveTo *move = [CCMoveTo actionWithDuration:1.f position:tutorialTitleTargetPoint];
    id easeMove = [CCEaseBackInOut actionWithAction:move];
    [tutorialTitle runAction: easeMove];
    
}

@end
