//
//  GameOver24.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver24.h"
#import "Level24.h"

@implementation GameOver24

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level24 alloc] initWithLevel24]];
}

@end
