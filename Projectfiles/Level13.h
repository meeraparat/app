//
//  Level13.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/2/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"

@interface Level13 : GameLayer
{
    CGFloat xStartRed;
    CGFloat xStartBlue;
    CGFloat xStartGreen;
    
    CCSprite * ground;
}
-(id) initWithLevel13;
+(Level13*) sharedLevel13;

@end
