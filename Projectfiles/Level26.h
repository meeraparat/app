//
//  Level26.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/8/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"

@interface Level26 : GameLayer
{
    NSMutableArray * rightBulletArray;
    NSMutableArray * leftBulletArray;
}
-(id) initWithLevel26;
+(Level26*) sharedLevel26;

@end
