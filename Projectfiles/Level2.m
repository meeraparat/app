//
//  Level2.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 7/31/13.
//
//

#import "Level2.h"
#import "NextLevel23.h"
#import "GameOver2.h"

@implementation Level2
{
    CCNode *extraHintTitle;
    CGPoint extraHintTitleTargetPoint;
    
    CCNode *arrowNode;
    CGPoint arrowTargetPoint;
}

static Level2 * instanceOfLevel2;



//If another class wants to get a reference to this layer, they can by calling this method
+(Level2*) sharedLevel2
{
	NSAssert(instanceOfLevel2 != nil, @"Level2 instance not yet initialized!");
	return instanceOfLevel2;
}

-(id) initWithLevel2
{
    self = [super init];
    if (self)
    {
        [self level2Mods];
        [self schedule:@selector(updateTwo:)];
    }
    return self;
}

-(void) level2Mods
{
    
    //this line initializes the instanceOfGameLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel2 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(251, 164, 65, 250)];
    [self addChild:colorLayer z:0];
    
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Click only the Red ones" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 2" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(55, (height - 25));
    [self addChild:levelDisplay];

    //        CCSprite *goodJobLabel = [CCSprite spriteWithFile:@"title.png"];
    CCLabelTTF * extraHint = [CCLabelTTF labelWithString:@"You must decipher the rules below.\n (hint: only click the button \n for the red objects)" fontName:@"Marker Felt" fontSize:20.0f];
    extraHintTitle = extraHint;
    //        }
    
    screenCenter = [CCDirector sharedDirector].screenCenter;
    screenSize = [CCDirector sharedDirector].screenSize;
    
    // place the goodJobTitle off-screen, later we will animate it on screen
    extraHintTitle.position = ccp (screenCenter.x, screenSize.height + 100);
    
    // this will be the point, we will animate the title to
    extraHintTitleTargetPoint = ccp(screenCenter.x, screenSize.height - 100);
    
    [self addChild:extraHintTitle z:1];
    
    CCSprite * arrow = [CCSprite spriteWithFile:@"greenArrow.png"];
    CCSprite * selectedArrow = [CCSprite spriteWithFile:@"greenArrow.png"];
    arrow = [CCMenuItemSprite itemWithNormalSprite:arrow selectedSprite:selectedArrow];
    
    arrowNode = arrow;
    arrowNode.position = ccp (screenCenter.x, screenSize.height + 100);
    arrowTargetPoint = ccp(screenCenter.x, screenSize.height - 190);
    
    [self addChild:arrowNode z:1];

    
}


-(void) updateTwo: (ccTime) dt
{
    
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel23 alloc] initWithMods]];
    }
    
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver2 alloc] init]];
}

-(void)onEnterTransitionDidFinish
{
    [super onEnterTransitionDidFinish];
    
    // animate the hint on to screen
    CCMoveTo *move = [CCMoveTo actionWithDuration:1.f position:extraHintTitleTargetPoint];
    id easeMove = [CCEaseBackInOut actionWithAction:move];
    [extraHintTitle runAction: easeMove];
    
    CCMoveTo *moveArrow = [CCMoveTo actionWithDuration:1.f position:arrowTargetPoint];
    id easeMoveArrow = [CCEaseBackInOut actionWithAction:moveArrow];
    [arrowNode runAction: easeMoveArrow];
    
}

-(void) processRightCollisions: (NSMutableArray*) arrayEntry
{
    CGRect RightRect = CGRectMake(rightButton.position.x - width/2, height/2 + rightButton.position.y, rightButton.contentSize.width, rightButton.contentSize.height);
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isRightClicked == TRUE)
        {
            if([[arrayEntry objectAtIndex:i] tag] == 0)
            {
                if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (RightRect.origin.y  + RightRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (RightRect.origin.y - RightRect.size.height/2 - 5)))
                {
                    ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                    [arrayEntry removeObjectAtIndex:i];
                    score = score + 10;
                }
            }
            if (([[arrayEntry objectAtIndex:i] tag] == 1) || ([[arrayEntry objectAtIndex:i] tag] == 2))
            {
                if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (RightRect.origin.y  + RightRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (RightRect.origin.y - RightRect.size.height/2 - 5)))
                {
                    score = score - 10;
                    CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                    CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:251 green:164 blue:65];
                    CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                    [colorLayer runAction: sequence];
                }
            }
        }
    }
    isRightClicked = FALSE;
}


-(void) processLeftCollisions: (NSMutableArray*) arrayEntry
{
    
    CGRect LeftRect = CGRectMake(leftButton.position.x - width/2, height/2 + leftButton.position.y, leftButton.contentSize.width, leftButton.contentSize.height);
    
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isLeftClicked == TRUE)
        {
            if([[arrayEntry objectAtIndex:i] tag] == 0)
            {
                if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (LeftRect.origin.y  + LeftRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (LeftRect.origin.y - LeftRect.size.height/2 - 5)))
                {
                    ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                    [arrayEntry removeObjectAtIndex:i];
                    score = score + 10;
                }
            }
            if (([[arrayEntry objectAtIndex:i] tag] == 1) || ([[arrayEntry objectAtIndex:i] tag] == 2))
            {
                if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (LeftRect.origin.y  + LeftRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (LeftRect.origin.y - LeftRect.size.height/2 - 5)))
                {
                    score = score - 10;
                    CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                    CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:251 green:164 blue:65];
                    CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                    [colorLayer runAction: sequence];
                }
            }
        }
    }
    isLeftClicked = FALSE;
}



@end
