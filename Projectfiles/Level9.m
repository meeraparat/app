//
//  Level9.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/1/13.
//
//

#import "Level9.h"
#import "NextLevel910.h"
#import "GameOver9.h"

@implementation Level9

static Level9 * instanceOfLevel9;



//If another class wants to get a reference to this layer, they can by calling this method
+(Level9*) sharedLevel9
{
	NSAssert(instanceOfLevel9 != nil, @"Level9 instance not yet initialized!");
	return instanceOfLevel9;
}

-(id) initWithLevel9
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateNine:)];
        [self level9mods];
    }
    return self;
}

-(void) level9mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel9 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(83, 2, 95, 250)];
    [self addChild:colorLayer z:0];
    
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Only click on the Blue ones" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 9" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(55, (height - 25));
    [self addChild:levelDisplay];

    
}

-(void) updateNine: (ccTime) dt
{
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel910 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver9 alloc] init]];
}

-(void) processRightCollisions: (NSMutableArray*) arrayEntry
{
    CGRect RightRect = CGRectMake(rightButton.position.x - width/2, height/2 + rightButton.position.y, rightButton.contentSize.width, rightButton.contentSize.height);
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isRightClicked == TRUE)
        {
            if ([[arrayEntry objectAtIndex:i] tag] == 1)
            {
                if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (RightRect.origin.y  + RightRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (RightRect.origin.y - RightRect.size.height/2 - 5)))
                {
                    ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                    [arrayEntry removeObjectAtIndex:i];
                    score = score + 10;
                }
            }
            if (([[arrayEntry objectAtIndex:i] tag] == 0) || ([[arrayEntry objectAtIndex:i] tag] == 2))
            {
                if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (RightRect.origin.y  + RightRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (RightRect.origin.y - RightRect.size.height/2 - 5)))
                {
                    score = score - 10;
                    CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                    CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:83 green:2 blue:95];
                    CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                    [colorLayer runAction: sequence];
                }
            }
        }
    }
    isRightClicked = FALSE;
}


-(void) processLeftCollisions: (NSMutableArray*) arrayEntry
{
    
    CGRect LeftRect = CGRectMake(leftButton.position.x - width/2, height/2 + leftButton.position.y, leftButton.contentSize.width, leftButton.contentSize.height);
    
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isLeftClicked == TRUE)
        {
            if ([[arrayEntry objectAtIndex:i] tag] == 1)
            {
                if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (LeftRect.origin.y  + LeftRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (LeftRect.origin.y - LeftRect.size.height/2 - 5)))
                {
                    ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                    [arrayEntry removeObjectAtIndex:i];
                    score = score + 10;
                }
            }
            if (([[arrayEntry objectAtIndex:i] tag] == 0) || ([[arrayEntry objectAtIndex:i] tag] == 2))
            {
                if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (LeftRect.origin.y  + LeftRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (LeftRect.origin.y - LeftRect.size.height/2 - 5)))
                {
                    score = score - 10;
                    CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                    CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:83 green:2 blue:95];
                    CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                    [colorLayer runAction: sequence];
                }
            }
        }
    }
    isLeftClicked = FALSE;
}


@end
