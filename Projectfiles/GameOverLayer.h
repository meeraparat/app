//
//  GameOverLayer.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 7/11/13.
//
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CCLayer.h"


@interface GameOverLayer : CCLayer
{
    CGFloat width;
    CGFloat height;
}

+(GameOverLayer*) sharedGameOverLayer;
-(void) doSomething;


@end
