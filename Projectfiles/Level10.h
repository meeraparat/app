//
//  Level10.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/1/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"

@interface Level10 : GameLayer

-(id) initWithLevel10;
+(Level10*) sharedLevel10;

@end
