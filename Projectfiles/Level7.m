//
//  Level3.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 7/31/13.
//
//

#import "Level7.h"
#import "NextLevel78.h"
#import "GameOver7.h"

@implementation Level7

static Level7 * instanceOfLevel7;

//If another class wants to get a reference to this layer, they can by calling this method
+(Level7*) sharedLevel7
{
	NSAssert(instanceOfLevel7 != nil, @"Level3 instance not yet initialized!");
	return instanceOfLevel7;
}

-(id) initWithLevel7
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateSeven:)];
        [self schedule: @selector(randomNumGenerator:) interval:((arc4random() % 5) + 3)];
        [self level7mods];
    }
    return self;
}

-(void) level7mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel7 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(252, 122, 105, 250)];
    [self addChild:colorLayer z:0];
    
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Follow the colors display" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 7" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(55, (height - 25));
    [self addChild:levelDisplay];
    
}

-(void) updateSeven: (ccTime) dt
{
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel78 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver7 alloc] init]];
}


-(void) moveFallingObjects: (NSMutableArray*) fallingObjectArray
{
    for(int i = 0; i < [fallingObjectArray count]; i++)
    {
        [[fallingObjectArray objectAtIndex:i] setPosition: ccp([(CCSprite*)[fallingObjectArray objectAtIndex:i] position].x, [(CCSprite*)[fallingObjectArray objectAtIndex:i] position].y - 2.5)];
    }
}

-(void) processRightCollisions: (NSMutableArray*) arrayEntry
{
    CGRect RightRect = CGRectMake(rightButton.position.x - width/2, height/2 + rightButton.position.y, rightButton.contentSize.width, rightButton.contentSize.height);
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isRightClicked == TRUE)
        {
            if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (RightRect.origin.y  + RightRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (RightRect.origin.y - RightRect.size.height/2 - 5)))
            {
                if ( (([[arrayEntry objectAtIndex:i] tag] == 0) && (isRedLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 1) && (isBlueLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 2) && (isGreenLabel == TRUE)))
                {
                    ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                    [arrayEntry removeObjectAtIndex:i];
                    score = score + 10;
                }
            }
        }
        else
        {
            if ((([[arrayEntry objectAtIndex:i] boundingBox].origin.y) < (RightRect.origin.y - RightRect.size.height/2 - 5)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > (RightRect.origin.y - RightRect.size.height/2 - 6.7)))
            {
                if ( (([[arrayEntry objectAtIndex:i] tag] == 0) && (isRedLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 1) && (isBlueLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 2) && (isGreenLabel == TRUE)))
                {
                    score = score - 10;
                    CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                    CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:252 green:122 blue:105];
                    CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                    [colorLayer runAction: sequence];                }
            }
        }
    }
    
    isRightClicked = FALSE;
}

-(void) processLeftCollisions: (NSMutableArray*) arrayEntry
{
    
    CGRect LeftRect = CGRectMake(leftButton.position.x - width/2, height/2 + leftButton.position.y, leftButton.contentSize.width, leftButton.contentSize.height);
    
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isLeftClicked == TRUE)
        {
            if (([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (LeftRect.origin.y  + LeftRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (LeftRect.origin.y - LeftRect.size.height/2 - 5)))
            {
                if ( (([[arrayEntry objectAtIndex:i] tag] == 0) && (isRedLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 1) && (isBlueLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 2) && (isGreenLabel == TRUE)))
                {
                    ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                    [arrayEntry removeObjectAtIndex:i];
                    score = score + 10;
                }
            }
        }
        else
        {
            if ([[arrayEntry objectAtIndex:i] boundingBox].origin.y < (LeftRect.origin.y - LeftRect.size.height/2 - 5) && [[arrayEntry objectAtIndex:i] boundingBox].origin.y > ((LeftRect.origin.y - LeftRect.size.height/2) - 6.7))
            {
                if ( (([[arrayEntry objectAtIndex:i] tag] == 0) && (isRedLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 1) && (isBlueLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 2) && (isGreenLabel == TRUE)))
                {
                    score = score - 10;
                    CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                    CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:252 green:122 blue:105];
                    CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                    [colorLayer runAction: sequence];                }
            }
        }
    }
    
    isLeftClicked = FALSE;
}


@end
