//
//  Level6.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 7/31/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"

@interface Level6 : GameLayer


-(id) initWithLevel6;
+(Level6*) sharedLevel6;

@end
