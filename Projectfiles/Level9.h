//
//  Level9.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/1/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"

@interface Level9 : GameLayer

-(id) initWithLevel9;
+(Level9*) sharedLevel9;

@end
