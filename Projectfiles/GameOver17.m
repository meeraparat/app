//
//  GameOver17.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver17.h"
#import "Level17.h"

@implementation GameOver17

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level17 alloc] initWithLevel17]];
}

@end
