//
//  Level15.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/4/13.
//
//

#import "Level15.h"
#import "NextLevel1516.h"
#import "GameOver15.h"

@implementation Level15

static Level15 * instanceOfLevel15;


//If another class wants to get a reference to this layer, they can by calling this method
+(Level15*) sharedLevel15
{
	NSAssert(instanceOfLevel15 != nil, @"Level15 instance not yet initialized!");
	return instanceOfLevel15;
}

-(id) initWithLevel15
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateFifteen:)];
        [self level15mods];
    }
    return self;
}

-(void) level15mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel15 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(250, 5, 197, 250)];
    [self addChild:colorLayer z:0];
    
    rightButton.position = ccp(-width, -(height * 10));
    leftButton.position = ccp(-width, -(height * 10));
    
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Stay clear of the Red" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 15" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(50, (height - 30));
    [self addChild:levelDisplay];
}


-(void) updateFifteen: (ccTime) dt
{    
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel1516 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver15 alloc] init]];
}

-(void) processRightCollisions: (NSMutableArray *) arrayEntry
{
    for (int i = 0; i < [arrayEntry count]; i++)
    {
        if(([[arrayEntry objectAtIndex:i] tag] == 1) || ([[arrayEntry objectAtIndex:i] tag] == 2))
        {
            CGRect Box = CGRectMake(([[rightFallingObjects objectAtIndex:i] boundingBox].origin.x - [[rightFallingObjects objectAtIndex:i] contentSize].width/2 - 5), ([[rightFallingObjects objectAtIndex:i] boundingBox].origin.y - 5), ([[rightFallingObjects objectAtIndex:i] contentSize].width + 10), ([[rightFallingObjects objectAtIndex:i]contentSize].height + 10));
            
            
            if ((CGRectContainsPoint(Box, pos)) )
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else if ([[arrayEntry objectAtIndex:i] tag] == 0)
        {
            CGRect Box = CGRectMake(([[rightFallingObjects objectAtIndex:i] boundingBox].origin.x - [[rightFallingObjects objectAtIndex:i] contentSize].width/2 - 5), ([[rightFallingObjects objectAtIndex:i] boundingBox].origin.y - 5), ([[rightFallingObjects objectAtIndex:i] contentSize].width + 10), ([[rightFallingObjects objectAtIndex:i]contentSize].height + 10));
            
            if (CGRectContainsPoint(Box, pos))
            {
                redClicked = TRUE;
            }
        }
    }
}


-(void) processLeftCollisions: (NSMutableArray *) arrayEntry
{
    for (int i = 0; i < [arrayEntry count]; i++)
    {
        if(([[arrayEntry objectAtIndex:i] tag] == 1) || ([[arrayEntry objectAtIndex:i] tag] == 2))
        {
            CGRect Box = CGRectMake(([[leftFallingObjects objectAtIndex:i] boundingBox].origin.x - [[leftFallingObjects objectAtIndex:i] contentSize].width/2 - 5), ([[leftFallingObjects objectAtIndex:i] boundingBox].origin.y - 5), ([[leftFallingObjects objectAtIndex:i] contentSize].width + 10), ([[leftFallingObjects objectAtIndex:i]contentSize].height + 10));
            
            
            if ((CGRectContainsPoint(Box, pos)) )
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else if ([[arrayEntry objectAtIndex:i] tag] == 0)
        {
            CGRect Box = CGRectMake(([[leftFallingObjects objectAtIndex:i] boundingBox].origin.x - [[leftFallingObjects objectAtIndex:i] contentSize].width/2 - 5), ([[leftFallingObjects objectAtIndex:i] boundingBox].origin.y - 5), ([[leftFallingObjects objectAtIndex:i] contentSize].width + 10), ([[leftFallingObjects objectAtIndex:i]contentSize].height + 10));
            
            if (CGRectContainsPoint(Box, pos))
            {
                redClicked = TRUE;
            }
        }
    }
}

-(void) moveFallingObjects: (NSMutableArray*) fallingObjectArray
{
    for(int i = 0; i < [fallingObjectArray count]; i++)
    {
        if (redClicked == TRUE)
        {
            [[fallingObjectArray objectAtIndex:i] setPosition: ccp([(CCSprite*)[fallingObjectArray objectAtIndex:i] position].x, [(CCSprite*)[fallingObjectArray objectAtIndex:i] position].y + 3)];
            
            [self runAction:[CCSequence actions:[CCDelayTime actionWithDuration:2], [CCCallFunc actionWithTarget:self selector:@selector(red)], nil]];

        }
        else
        {
            [[fallingObjectArray objectAtIndex:i] setPosition: ccp([(CCSprite*)[fallingObjectArray objectAtIndex:i] position].x, [(CCSprite*)[fallingObjectArray objectAtIndex:i] position].y - 2)];
        }
    }
}

-(void) red
{
    redClicked = FALSE;
}



@end
