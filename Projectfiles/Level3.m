//
//  Level3.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 7/31/13.
//
//

#import "Level3.h"
#import "NextLevel34.h"
#import "GameOver3.h"

@implementation Level3

static Level3 * instanceOfLevel3;

//If another class wants to get a reference to this layer, they can by calling this method
+(Level3*) sharedLevel3
{
	NSAssert(instanceOfLevel3 != nil, @"Level3 instance not yet initialized!");
	return instanceOfLevel3;
}

-(id) initWithLevel3
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateThree:)];
        [self level3mods];
    }
    return self;
}

-(void) level3mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel3 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(251, 50, 100, 250)];
    [self addChild:colorLayer z:0];
    
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Faster" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 3" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(55, (height - 25));
    [self addChild:levelDisplay];
    
}

-(void) updateThree: (ccTime) dt
{
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel34 alloc] initWithMods]];
    }    
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver3 alloc] init]];
}


-(void) moveFallingObjects: (NSMutableArray*) fallingObjectArray
{
    for(int i = 0; i < [fallingObjectArray count]; i++)
    {
        [[fallingObjectArray objectAtIndex:i] setPosition: ccp([(CCSprite*)[fallingObjectArray objectAtIndex:i] position].x, [(CCSprite*)[fallingObjectArray objectAtIndex:i] position].y - 2.5)];
    }
}

-(void) processRightCollisions: (NSMutableArray*) arrayEntry
{
    CGRect RightRect = CGRectMake(rightButton.position.x - width/2, height/2 + rightButton.position.y, rightButton.contentSize.width, rightButton.contentSize.height);
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isRightClicked == TRUE)
        {
            if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (RightRect.origin.y  + RightRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (RightRect.origin.y - RightRect.size.height/2 - 5)))
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else
        {
            if ((([[arrayEntry objectAtIndex:i] boundingBox].origin.y) < (RightRect.origin.y - RightRect.size.height/2 - 5)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > (RightRect.origin.y - RightRect.size.height/2 - 7)))
            {
                score = score - 10;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:251 green:50 blue:100];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
            }
        }
    }
    
    isRightClicked = FALSE;
}


-(void) processLeftCollisions: (NSMutableArray*) arrayEntry
{
    
    CGRect LeftRect = CGRectMake(leftButton.position.x - width/2, height/2 + leftButton.position.y, leftButton.contentSize.width, leftButton.contentSize.height);
    
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isLeftClicked == TRUE)
        {
            if (([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (LeftRect.origin.y  + LeftRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (LeftRect.origin.y - LeftRect.size.height/2 - 5)))
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else
        {
            if ([[arrayEntry objectAtIndex:i] boundingBox].origin.y < (LeftRect.origin.y - LeftRect.size.height/2 - 5) && [[arrayEntry objectAtIndex:i] boundingBox].origin.y > ((LeftRect.origin.y - LeftRect.size.height/2) - 7))
            {
                score = score - 10;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:251 green:50 blue:100];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
                
            }
        }
    }
    
    isLeftClicked = FALSE;
}




@end
