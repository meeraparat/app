/*
 * Kobold2D™ --- http://www.kobold2d.org
 *
 * Copyright (c) 2010-2011 Steffen Itterheim, Andreas Loew 
 * Released under MIT License in Germany (LICENSE-Kobold2D.txt).
 */

//  Updated by Andreas Loew on 20.06.11:
//  * retina display
//  * framerate independency
//  * using TexturePacker http://www.texturepacker.com

#import "GameLayer.h"
#import "Entity.h"
#import "GenericMenuLayer.h"
#import "GameOverLayer.h"
#import "FallingObjects.h"
#import "NextLevel.h"
#import "SimpleAudioEngine.h"
#import "Level2.h"


@implementation GameLayer
{
//    CCNode * appearingHint;
//    CGPoint appearingHintTargetPoint;
}

static CGRect screenRect;
static int coinCounter;
static GameLayer* instanceOfGameLayer;

//These labels will be used to check if touch input is working
//**COMMENT THIS OUT WHEN YOU START WORK ON YOUR OWN GAME
CCLabelTTF* verifyTouchStart;
CCLabelTTF* verifyTouchAvailable;
CCLabelTTF* verifyTouchEnd;
//**


CCMenu * buttonMenu;


//this allows other classes in your project to query the GameLayer for the screenRect
+(CGRect) screenRect
{
	return screenRect;
}

//If another class wants to get a reference to this layer, they can by calling this method
+(GameLayer*) sharedGameLayer
{
	NSAssert(instanceOfGameLayer != nil, @"GameLayer instance not yet initialized!");
	return instanceOfGameLayer;
}

-(id) init
{
	if ((self = [super init]))
	{
        //this line initializes the instanceOfGameLayer variable such that it can be accessed by the sharedGameLayer method
		instanceOfGameLayer = self;

        //get the rectangle that describes the edges of the screen
		CGSize screenSize = [[CCDirector sharedDirector] winSize];
		screenRect = CGRectMake(0, 0, screenSize.width, screenSize.height);
        
        // 480 x 320
        width = screenRect.size.width;
        height = screenRect.size.height;

        colorLayer = [CCLayerColor layerWithColor:ccc4(20, 64, 92, 250)];
        [self addChild:colorLayer z:0];
        
		
        hint = [CCLabelTTF labelWithString:@"" fontName:@"Marker Felt" fontSize:20.0f];
        hint.position = ccp(width/2, 25);
        [self addChild:hint z:3];
//        
//        
//        rulesHelpLabel = [CCLabelTTF labelWithString:@"Read the hint first!" fontName:@"Marker Felt" fontSize:20.0f];
////        CCMenuItemLabel * rulesHelp = [CCMenuItemLabel itemWithLabel:rulesHelpLabel];
//        appearingHint = rulesHelpLabel;
//        
//        
//        arrow = [CCMenuItemImage itemWithNormalImage:@"greenArrow.png" selectedImage:@"greenArrow.png"];
//        arrow.position = ccp(0, -75);
//        
//        appearingHintMenu = [CCMenu menuWithItems:arrow, nil];
//        [self addChild:appearingHintMenu z:2];
//        //        appearingHint = appearingHintMenu;
        
        screenCenter = [CCDirector sharedDirector].screenCenter;
//        screenSize = [CCDirector sharedDirector].screenSize;

        // place the appearingHint off-screen, later we will animate it on screen
//        appearingHint.position = ccp (screenCenter.x, screenSize.height + 100);
        
        // this will be the point, we will animate the title to
//        appearingHintTargetPoint = ccp(screenCenter.x, screenSize.height - 200);
        
//		[self addChild:appearingHint z:3];

        
        //This puts a ship on screen so you know you've switched to this layer and everything is loading right
        //**COMMENT THIS OUT WHEN YOU START WORK ON YOUR OWN GAME
        
//        testEntity = [Entity createEntity];
//        [testEntity setPosition: ccp(screenSize.width/2, screenSize.height/2)];
//        [self addChild:testEntity z:1 tag:1];
        
        //**
        
        //We've provided this code so you can check to see if touch input is working.
        //**COMMENT THIS OUT WHEN YOU START WORK ON YOUR OWN GAME
        verifyTouchStart = [CCLabelTTF labelWithString:@"Touch Started" fontName:@"arial" fontSize:20.0f];
        verifyTouchAvailable = [CCLabelTTF labelWithString:@"No Taps" fontName:@"arial" fontSize:16.0f];
        verifyTouchEnd = [CCLabelTTF labelWithString:@"Touch Ended" fontName:@"arial" fontSize:20.0f];

        verifyTouchStart.position = ccp(1000,1000);
        verifyTouchAvailable.position = ccp(1600,3000);
        verifyTouchEnd.position = ccp(4000,1000);
        
        
        verifyTouchStart.visible = false;
        verifyTouchEnd.visible = false;
        touched = false;
        
        
        [self addChild: verifyTouchStart z:1 tag: TouchStartedLabelTag];
        [self addChild: verifyTouchAvailable z:1 tag: TouchAvailableLabelTag];
        [self addChild: verifyTouchEnd z:1 tag: TouchEndedLabelTag];
        //**
        
        //This will schedule a call to the update method every frame
        [self scheduleUpdate];
        [self schedule: @selector(spawnFallingObject:) interval:3];
        
        
        mainCreature = [CCSprite spriteWithFile:@"penguin.png"];
//        [self addChild:mainCreature z:1];
        float creatureWidth = mainCreature.contentSize.width;
        float creatureHeight = mainCreature.contentSize.height;
//        NSLog(@"creature width %f", creatureWidth);
        mainCreature.anchorPoint = ccp(0.5, 0);
        mainCreature.position = ccp(screenSize.width/2,10);
        
        scoreLabel = [CCLabelTTF labelWithString: [NSString stringWithFormat:@"%d", score] dimensions: CGSizeMake(200, 30) alignment: UITextAlignmentRight fontName:@"Marker Felt" fontSize: 28];
        [scoreLabel setPosition: ccp((width - 125) ,(height - 50))];
        [self addChild:scoreLabel z:3];

        levelDisplay = [CCLabelTTF labelWithString:@"Level 1" fontName:@"Marker Felt" fontSize:20];
        levelDisplay.position = ccp(55, (height - 25));
        [self addChild:levelDisplay];
        
        CCMenuItemImage * menuButton = [CCMenuItemImage itemWithNormalImage:@"menuButton.jpg" selectedImage:@"menuButton.jpg" target:self selector:@selector(backToMenu:)];
        menuButton.position = ccp(0, 130);
        CCMenu * backToMenu = [CCMenu menuWithItems:menuButton, nil];
        [self addChild: backToMenu z:3];
        
        rightButton = [CCMenuItemImage itemWithNormalImage:@"bar_button.jpg" selectedImage:@"bar_button.jpg" target:self selector:@selector(rightClicked:)];
        rightButton.position = ccp(150, -75);
        
        leftButton = [CCMenuItemImage itemWithNormalImage:@"bar_button.jpg" selectedImage:@"bar_button.jpg" target:self selector:@selector(leftClicked:)];
        leftButton.position = ccp(-150, -75);
        
        CCMenu *buttonMenu = [CCMenu menuWithItems:rightButton, leftButton, nil];
        [self addChild:buttonMenu z:3];
        
        CCMenuItemImage * skipButton = [CCMenuItemImage itemWithNormalImage:@"skipButton.gif" selectedImage:@"skipButton.gif" target:self selector:@selector(skipClicked:)];
        skipButton.position = ccp(width/2 - 50, height/2 - 20);
        CCMenu * skipMenu = [CCMenu menuWithItems:skipButton, nil];
        [self addChild:skipMenu z:2];
                
        
        //boundingBox is the bottom edge of the rect. boundingBoxCenter is the center of the rect.

        rightFallingObjects = [[NSMutableArray alloc] init];
        leftFallingObjects = [[NSMutableArray alloc] init];

        CCSprite * star = [CCSprite spriteWithFile:@"starIcon.png"];
        star.position = ccp(65, height - 50);
        [self addChild:star z:3];
        
//        coinCounter = [NSNumber numberWithInt: [[[NSUserDefaults standardUserDefaults] objectForKey:@"coinCounter"] integerValue]];
        coinCounter = [[[NSUserDefaults standardUserDefaults] objectForKey:@"coinCounter"] intValue];
        CCLabelTTF *numberOfCoins = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", coinCounter] fontName:@"Marker Felt" fontSize:12];
        numberOfCoins.position = ccp(45, height - 50);
        [self addChild:numberOfCoins z:3];
        NSLog(@"saved counter at start is %d", coinCounter);
        
        
    }
	return self;
}

-(void)onEnterTransitionDidFinish
{
    [super onEnterTransitionDidFinish];
    
    // animate the title on to screen
//    CCMoveTo *move = [CCMoveTo actionWithDuration:1.f position:appearingHintTargetPoint];
//    id easeMove = [CCEaseBackInOut actionWithAction:move];
//    [appearingHint runAction: easeMove];
    
}

-(void) backToMenu: (CCMenuItemImage *) menuButton
{
    [[CCDirector sharedDirector] replaceScene: [[GenericMenuLayer alloc] init]];
}

-(void) skipClicked: (CCMenuItemImage *) skipLevelButton
{
//    coinCounter = [NSNumber numberWithInt: [[[NSUserDefaults standardUserDefaults] objectForKey:@"coinCounter"] integerValue]];
    
    if (coinCounter >= 2)
    {
//        coinCounter = [NSNumber numberWithInt:[coinCounter integerValue] - 3];
        coinCounter -= 3;
        savedCoinCounter = [NSNumber numberWithInt:coinCounter];
        [[NSUserDefaults standardUserDefaults] setObject:savedCoinCounter forKey:@"coinCounter"];
        [[NSUserDefaults standardUserDefaults] synchronize];
//        NSLog(@"%@", coinCounter);

        score = 110;
    }
}


- (NSArray *)generate:(int)n randomUniqueNumbersBetween:(int)lowerLimit upperLimit:(int)upperLimit rangeOccupation:(int)rangeOccupation
{
  NSMutableArray *randomNumbers = [NSMutableArray array];
  NSMutableArray *randomNumberRanges = [NSMutableArray array];
  
  for (int i=0; i<n ; i++) {
   
    BOOL randomNumberUnique = YES;
    int randomNumber = 0;
    NSRange randomNumberRange = NSMakeRange(0, 0);
    
    do {
      // generate random number
      randomNumber = arc4random_uniform(upperLimit-lowerLimit) + lowerLimit;
      // calculate range for random number
      randomNumberRange = NSMakeRange(randomNumber, rangeOccupation);
    
      // check if random number is in already existing range
      for (NSValue *rangeValue in randomNumberRanges) {
        NSRange range = [rangeValue rangeValue];
        
        // calculate intersection of existing range with new range
        NSRange intersectionRange = NSIntersectionRange(randomNumberRange, range);
        randomNumberUnique = (intersectionRange.length > 0) ? NO : YES;
        if (!randomNumberUnique) {
          break;
        }
      }
    } while (!randomNumberUnique);
    
    [randomNumberRanges addObject:[NSValue valueWithRange:randomNumberRange]];
    [randomNumbers addObject:@(randomNumber)];
  }
  
  return randomNumbers;
}

-(void) spawnFallingObject: (ccTime) dt
{
  
    FallingObjects * redRight = [FallingObjects getType:0];
    FallingObjects * blueRight = [FallingObjects getType:1];
    FallingObjects * greenRight = [FallingObjects  getType:2];
    
    FallingObjects * redLeft = [FallingObjects getType:0];
    FallingObjects * blueLeft = [FallingObjects getType:1];
    FallingObjects * greenLeft = [FallingObjects  getType:2];
  
    NSArray *rightStartPositions = [self generate:3 randomUniqueNumbersBetween:0 upperLimit:250 rangeOccupation:redRight.contentSize.height];

    yStartRedRight = height + [rightStartPositions[0] intValue];
    yStartBlueRight = height + [rightStartPositions[1] intValue];
    yStartGreenRight = height + [rightStartPositions[2] intValue];
  
    NSArray *leftStartPositions = [self generate:3 randomUniqueNumbersBetween:0 upperLimit:250 rangeOccupation:redLeft.contentSize.height];
  
    yStartRedLeft = height + [leftStartPositions[0] intValue];
    yStartBlueLeft = height + [leftStartPositions[1] intValue];
    yStartGreenLeft = height + [leftStartPositions[2] intValue];
  
    [self addChild:redRight z:1 tag:0];
    redRight.position = ccp(width/2 + 150, yStartRedRight);
    
    [self addChild:blueRight z:1 tag:1];
    blueRight.position = ccp(width/2 + 150, yStartBlueRight);
    
    [self addChild:greenRight z:1 tag:2];
    greenRight.position = ccp(width/2 + 150, yStartGreenRight);

    ////////////////////
    
    [self addChild:redLeft z:1 tag:0];
    redLeft.position = ccp(width/2 - 150, yStartRedLeft);
    
    [self addChild:blueLeft z:1 tag:1];
    blueLeft.position = ccp(width/2 - 150, yStartBlueLeft);
    
    [self addChild:greenLeft z:1 tag:2];
    greenLeft.position = ccp(width/2 - 150, yStartGreenLeft);


    
    [rightFallingObjects addObject:redRight];
    [rightFallingObjects addObject:blueRight];
    [rightFallingObjects addObject:greenRight];
    
    [leftFallingObjects addObject:redLeft];
    [leftFallingObjects addObject:blueLeft];
    [leftFallingObjects addObject:greenLeft];
}


-(void) update: (ccTime) dt
{
     
    [self moveFallingObjects:rightFallingObjects];
    [self moveFallingObjects:leftFallingObjects];
    
    input = [KKInput sharedInput];
        
    //This will be true during the frame a new finger touches the screen
    if(input.anyTouchBeganThisFrame)
    {
        //This lets you see if the touch was registered
        //**COMMENT THIS OUT WHEN YOU START WORK ON YOUR OWN GAME
        [self getChildByTag:TouchStartedLabelTag].visible = true;
        [self getChildByTag:TouchEndedLabelTag].visible = false;
    }
    
    //This will be true as long as there is at least one finger touching the screen
    if(input.touchesAvailable)
    {
        //This lets you see where you are touching
        //**COMMENT THIS OUT WHEN YOU START WORK ON YOUR OWN GAME
        pos = [input locationOfAnyTouchInPhase:KKTouchPhaseAny];
        [self getChildByTag:TouchEndedLabelTag].visible = false;
//        [((CCLabelTTF*)[self getChildByTag:TouchAvailableLabelTag]) setString:[NSString stringWithFormat:@"You are tapping at %@", NSStringFromCGPoint(pos) ]];
        //**
    }
    
    //This will be true during the frame a finger that was once touching the screen stops touching the screen
    if(input.anyTouchEndedThisFrame)
    {
        //This lets you see if the end of the touch was registered
        //**COMMENT THIS OUT WHEN YOU START WORK ON YOUR OWN GAME
        [self getChildByTag:TouchEndedLabelTag].visible = true;
        pos = ccp(-15, -15);
        touchCounter++;
        touched = true;

    }
    
    [self processRightCollisions:rightFallingObjects];
    [self processLeftCollisions:leftFallingObjects];
    
    [self updateScore:score];
    
    if (score >= 100)
    {
//        coinCounter = [NSNumber numberWithInt: [[[NSUserDefaults standardUserDefaults] objectForKey:@"coinCounter"] integerValue] + 1];
        coinCounter++;
        savedCoinCounter = [NSNumber numberWithInteger:coinCounter];
        [[NSUserDefaults standardUserDefaults] setObject:savedCoinCounter forKey:@"coinCounter"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"%d", [savedCoinCounter intValue]);
//        [self removeChild:appearingHintMenu];
//        NSLog(@"removing arrow...");
        [[CCDirector sharedDirector] replaceScene: [[NextLevel alloc] init]];
    }
    
    if (score <= -100)
    {
        [self runAction:[CCSequence actions:[CCDelayTime actionWithDuration:0.5], [CCCallFunc actionWithTarget:self selector:@selector(switchScene)], nil]];
    }
    
}

-(void) updateScore:(int) newScore
{
    [scoreLabel setString: [NSString stringWithFormat:@"%d", newScore]];
}


-(void) dealloc
{
	instanceOfGameLayer = nil;
	
#ifndef KK_ARC_ENABLED
	// don't forget to call "super dealloc"
	[super dealloc];
#endif // KK_ARC_ENABLED
}


-(void) moveFallingObjects: (NSMutableArray*) fallingObjectArray
{
    for(int i = 0; i < [fallingObjectArray count]; i++)
    {
        [[fallingObjectArray objectAtIndex:i] setPosition: ccp([(CCSprite*)[fallingObjectArray objectAtIndex:i] position].x, [(CCSprite*)[fallingObjectArray objectAtIndex:i] position].y - 1.75)];
    }
}


-(void) processRightCollisions: (NSMutableArray*) arrayEntry
{
    CGRect RightRect = CGRectMake(rightButton.position.x - width/2, height/2 + rightButton.position.y, rightButton.contentSize.width, rightButton.contentSize.height);
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isRightClicked == TRUE)
        {
            if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (RightRect.origin.y  + RightRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (RightRect.origin.y - RightRect.size.height/2 - 5)))
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else
        {
            if ((([[arrayEntry objectAtIndex:i] boundingBox].origin.y) < (RightRect.origin.y - RightRect.size.height/2 - 5)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > (RightRect.origin.y - RightRect.size.height/2 - 7)))
            {
//                missed++;
//                degrees = missed * 5;
//                [mainCreature setRotation: degrees];
                score = score - 10;
//                [self deadDetector];
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:20 green:64 blue:92];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
            }
        }
    }
    
    isRightClicked = FALSE;
}


-(void) processLeftCollisions: (NSMutableArray*) arrayEntry
{
  
    CGRect LeftRect = CGRectMake(leftButton.position.x - width/2, height/2 + leftButton.position.y, leftButton.contentSize.width, leftButton.contentSize.height);
    
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isLeftClicked == TRUE)
        {
            if (([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (LeftRect.origin.y  + LeftRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (LeftRect.origin.y - LeftRect.size.height/2 - 5)))
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else
        {
            if ([[arrayEntry objectAtIndex:i] boundingBox].origin.y < (LeftRect.origin.y - LeftRect.size.height/2 - 5) && [[arrayEntry objectAtIndex:i] boundingBox].origin.y > ((LeftRect.origin.y - LeftRect.size.height/2) - 7))
            {
//                missed--;
//                degrees = missed * 5;
//                [mainCreature setRotation: degrees];
                score = score - 10;
//                [self deadDetector];
//                CCLayerColor * baseColor = colorLayer;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:20 green:64 blue:92];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
                
            }
        }
    }
    
    isLeftClicked = FALSE;
}



-(void) rightClicked: (CCMenuItemImage *) rightbutton
{
    isRightClicked = TRUE;
}


-(void) leftClicked: (CCMenuItemImage *) leftButton
{
    isLeftClicked = TRUE;
}


//-(void) deadDetector
//{
//    if (degrees > 45)
//    {
//        
//        [mainCreature setRotation:90];
//        //QUITS GAME... so its not still jumping up n down, etc
//        //[[self moveFallingObjects:rightFallingObjects] remo
//        [self removeChildByTag:0];
//        [self removeChildByTag:1];
//        [self removeChildByTag:2];
//        for (int i = 0; i < (int)[rightFallingObjects count]; i++)
//        {
//            ((CCSprite*)[rightFallingObjects objectAtIndex:i]).visible = FALSE;
//            [rightFallingObjects removeObjectAtIndex:i];
//        }
//        for (int i = 0; i < (int)[leftFallingObjects count]; i++)
//        {
//            ((CCSprite*)[leftFallingObjects objectAtIndex:i]).visible = FALSE;
//            [leftFallingObjects removeObjectAtIndex:i];
//        }
//        
//
//        [self runAction:[CCSequence actions:[CCDelayTime actionWithDuration:2], [CCCallFunc actionWithTarget:self selector:@selector(switchScene)], nil]];
//    }
//    
//    else
//        if (degrees < -45)
//        {
//            [mainCreature setRotation:-90];
//            [self removeChildByTag:0];
//            [self removeChildByTag:1];
//            [self removeChildByTag:2];
//            for (int i = 0; i < (int)[rightFallingObjects count]; i++)
//            {
//                ((CCSprite*)[rightFallingObjects objectAtIndex:i]).visible = FALSE;
//                [rightFallingObjects removeObjectAtIndex:i];
//            }
//            for (int i = 0; i < (int)[leftFallingObjects count]; i++)
//            {
//                ((CCSprite*)[leftFallingObjects objectAtIndex:i]).visible = FALSE;
//                [leftFallingObjects removeObjectAtIndex:i];
//            }
//            
//
//            [self runAction:[CCSequence actions:[CCDelayTime actionWithDuration:2], [CCCallFunc actionWithTarget:self selector:@selector(switchScene)], nil]];
//        }
//}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOverLayer alloc] init]];
}


@end
