//
//  GameOver25.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver25.h"
#import "Level25.h"

@implementation GameOver25

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level25 alloc] initWithLevel25]];
}

@end
