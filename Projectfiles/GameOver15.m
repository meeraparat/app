//
//  GameOver15.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver15.h"
#import "Level15.h"

@implementation GameOver15

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level15 alloc] initWithLevel15]];
}

@end
