//
//  Level19.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/5/13.
//
//

#import "Level19.h"
#import "NextLevel1920.h"
#import "GameOver19.h"

@implementation Level19


static Level19 * instanceOfLevel19;


//If another class wants to get a reference to this layer, they can by calling this method
+(Level19*) sharedLevel19
{
	NSAssert(instanceOfLevel19 != nil, @"Level19 instance not yet initialized!");
	return instanceOfLevel19;
}

-(id) initWithLevel19
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateNineteen:)];
        [self schedule:@selector(buttonPositionChangeTimer:) interval:3];
        [self level19mods];
    }
    return self;
}

-(void) level19mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel19 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(251, 79, 40, 250)];
    [self addChild:colorLayer z:0];
        
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"The buttons will keep moving" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 19" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(50, (height - 30));
    [self addChild:levelDisplay];
    
}


-(void) updateNineteen: (ccTime) dt
{
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel1920 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver19 alloc] init]];
}

-(void) buttonPositionChangeTimer: (ccTime) dt
{
    rightButton.position = ccp(150, ((arc4random() % 50)));
    leftButton.position = ccp(-150, ((arc4random() % 50)));
}

-(void) processRightCollisions: (NSMutableArray*) arrayEntry
{
    CGRect RightRect = CGRectMake(rightButton.position.x - width/2, height/2 + rightButton.position.y, rightButton.contentSize.width, rightButton.contentSize.height);
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isRightClicked == TRUE)
        {
            if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (RightRect.origin.y  + RightRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (RightRect.origin.y - RightRect.size.height/2 - 5)))
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else
        {
            if ((([[arrayEntry objectAtIndex:i] boundingBox].origin.y) < (RightRect.origin.y - RightRect.size.height/2 - 5)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > (RightRect.origin.y - RightRect.size.height/2 - 7)))
            {
                score = score - 10;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:251 green:79 blue:40];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
            }
        }
    }
    
    isRightClicked = FALSE;
}


-(void) processLeftCollisions: (NSMutableArray*) arrayEntry
{
    
    CGRect LeftRect = CGRectMake(leftButton.position.x - width/2, height/2 + leftButton.position.y, leftButton.contentSize.width, leftButton.contentSize.height);
    
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isLeftClicked == TRUE)
        {
            if (([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (LeftRect.origin.y  + LeftRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (LeftRect.origin.y - LeftRect.size.height/2 - 5)))
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else
        {
            if ([[arrayEntry objectAtIndex:i] boundingBox].origin.y < (LeftRect.origin.y - LeftRect.size.height/2 - 5) && [[arrayEntry objectAtIndex:i] boundingBox].origin.y > ((LeftRect.origin.y - LeftRect.size.height/2) - 7))
            {
                score = score - 10;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:251 green:79 blue:40];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
                
            }
        }
    }
    
    isLeftClicked = FALSE;
}


@end