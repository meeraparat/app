//
//  Level4.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 7/31/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"

@interface Level4 : GameLayer
{
    int colorDecider;
    
    CCLabelTTF * redLabel;
    CCLabelTTF * blueLabel;
    CCLabelTTF * greenLabel;
    
    BOOL isRedLabel;
    BOOL isBlueLabel;
    BOOL isGreenLabel;
}

-(id) initWithLevel4;
+(Level4*) sharedLevel4;

@end
