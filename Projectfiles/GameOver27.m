//
//  GameOver27.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver27.h"
#import "Level27.h"

@implementation GameOver27

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level27 alloc] initWithLevel27]];
}

@end
