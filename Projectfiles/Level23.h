//
//  Level23.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/7/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"

@interface Level23 : GameLayer
{
    int colorDecider;
    
    CCLabelTTF * redLabel;
    CCLabelTTF * blueLabel;
    CCLabelTTF * greenLabel;
    
    BOOL isRedLabel;
    BOOL isBlueLabel;
    BOOL isGreenLabel;

}

-(id) initWithLevel23;
+(Level23*) sharedLevel23;

@end
