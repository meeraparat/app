//
//  Level5.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 7/31/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"

@interface Level5 : GameLayer
{
    
}

-(id) initWithLevel5;
+(Level5*) sharedLevel5;

@end
