//
//  GameOver18.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver18.h"
#import "Level18.h"

@implementation GameOver18

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level18 alloc] initWithLevel18]];
}

@end
