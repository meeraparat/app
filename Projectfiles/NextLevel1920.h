//
//  NextLevel1920.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/7/13.
//
//

#import <Foundation/Foundation.h>
#import "NextLevel.h"

@interface NextLevel1920 : NextLevel

-(id) initWithMods;

@end
