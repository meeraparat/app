//
//  GameOver22.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver22.h"
#import "Level22.h"

@implementation GameOver22

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level22 alloc] initWithLevel22]];
}

@end
