//
//  Level16.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/5/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"
#import "Level2.h"

@interface Level16 : Level2

-(id) initWithLevel16;
+(Level16*) sharedLevel16;

@end
