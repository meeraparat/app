//
//  GameOver7.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver7.h"
#import "Level7.h"

@implementation GameOver7

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level7 alloc] initWithLevel7]];
}

@end
