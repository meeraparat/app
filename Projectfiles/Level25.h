//
//  Level25.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/8/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"
#import "Level13.h"

@interface Level25 : Level13

-(id) initWithLevel25;
+(Level25*) sharedLevel25;

@end
