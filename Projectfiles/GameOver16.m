//
//  GameOver16.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver16.h"
#import "Level16.h"

@implementation GameOver16

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level16 alloc] initWithLevel16]];
}

@end
