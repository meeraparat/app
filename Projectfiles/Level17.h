//
//  Level17.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/5/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"

@interface Level17 : GameLayer

-(id) initWithLevel17;
+(Level17*) sharedLevel17;

@end
