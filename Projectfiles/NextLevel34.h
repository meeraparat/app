//
//  NextLevel34.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 7/30/13.
//
//

#import <Foundation/Foundation.h>
#import "NextLevel.h"

@interface NextLevel34 : NextLevel

-(id) initWithMods;

@end
