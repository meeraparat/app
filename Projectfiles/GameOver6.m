//
//  GameOver6.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver6.h"
#import "Level6.h"

@implementation GameOver6

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level6 alloc] initWithLevel6]];
}

@end
