//
//  Level13.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/2/13.
//
//

#import "Level13.h"
#import "NextLevel1314.h"
#import "GameOver13.h"

@implementation Level13

static Level13 * instanceOfLevel13;



//If another class wants to get a reference to this layer, they can by calling this method
+(Level13*) sharedLevel13
{
	NSAssert(instanceOfLevel13 != nil, @"Level13 instance not yet initialized!");
	return instanceOfLevel13;
}

-(id) initWithLevel13
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateThirteen:)];
        [self level13mods];
    }
    return self;
}

-(void) level13mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel13 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(253, 211, 140, 250)];
    [self addChild:colorLayer z:0];
    
    rightButton.position = ccp(-width, -(height * 10));
    leftButton.position = ccp(-width, -(height * 10));
    
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Click them before they touch the ground!" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 13" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(50, (height - 30));
    [self addChild:levelDisplay];
    
//    ground = [CCSprite spriteWithFile:@"ground.jpeg"];
//    ground.position = ccp(width/2, (ground.contentSize.height/2));
//    [self addChild:ground z:2];
    
    [self removeChild:mainCreature];
}

-(void) spawnFallingObject: (ccTime) dt
{
    yStartRedRight = height + (arc4random() % 400);
    yStartBlueRight = height + (arc4random() % 400);
    yStartGreenRight = height + (arc4random() % 400);
    
    yStartRedLeft = height + (arc4random() % 400);
    yStartBlueLeft = height + (arc4random() % 400);
    yStartGreenLeft = height + (arc4random() % 400);
    
    xStartRed = (arc4random() % 480);
    xStartBlue = (arc4random() % 480);
    xStartGreen = (arc4random() % 480);

    
    FallingObjects * redRight = [FallingObjects getType:0];
    FallingObjects * blueRight = [FallingObjects getType:1];
    FallingObjects * greenRight = [FallingObjects  getType:2];
    
    FallingObjects * redLeft = [FallingObjects getType:0];
    FallingObjects * blueLeft = [FallingObjects getType:1];
    FallingObjects * greenLeft = [FallingObjects  getType:2];
    
    
    
    [self addChild:redRight z:1 tag:0];
    redRight.position = ccp(xStartRed, yStartRedRight);
    
    [self addChild:blueRight z:1 tag:1];
    blueRight.position = ccp(xStartBlue, yStartBlueRight);
    
    [self addChild:greenRight z:1 tag:2];
    greenRight.position = ccp(xStartGreen, yStartGreenRight);
    
    ////////////////////
    
    [self addChild:redLeft z:1 tag:0];
    redLeft.position = ccp(xStartRed, yStartRedLeft);
    
    [self addChild:blueLeft z:1 tag:1];
    blueLeft.position = ccp(xStartBlue, yStartBlueLeft);
    
    [self addChild:greenLeft z:1 tag:2];
    greenLeft.position = ccp(xStartGreen, yStartGreenLeft);
    
    
    [rightFallingObjects addObject:redRight];
    [rightFallingObjects addObject:blueRight];
    [rightFallingObjects addObject:greenRight];
    
    [leftFallingObjects addObject:redLeft];
    [leftFallingObjects addObject:blueLeft];
    [leftFallingObjects addObject:greenLeft];    
    
}


-(void) updateThirteen: (ccTime) dt
{
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel1314 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver13 alloc] init]];
}


-(void) processRightCollisions: (NSMutableArray *) arrayEntry
{
    for (int i = 0; i < [arrayEntry count]; i++)
    {
        CGRect Box = CGRectMake(([[rightFallingObjects objectAtIndex:i] boundingBox].origin.x - [[rightFallingObjects objectAtIndex:i] contentSize].width/2 - 5), ([[rightFallingObjects objectAtIndex:i] boundingBox].origin.y - 5), ([[rightFallingObjects objectAtIndex:i] contentSize].width + 10), ([[rightFallingObjects objectAtIndex:i]contentSize].height + 10));
        
        if ((CGRectContainsPoint(Box, pos)) )
        {
            ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
            [arrayEntry removeObjectAtIndex:i];
            score = score + 10;
        }
        else
        {
            if (([[arrayEntry objectAtIndex:i] boundingBox].origin.y < -5) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > -6.5))
            {
                score = score - 10;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:253 green:211 blue:140];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
                
            }
        }
        

    }
}


-(void) processLeftCollisions: (NSMutableArray *) arrayEntry
{
    for (int i = 0; i < [arrayEntry count]; i++)
    {
        CGRect Box = CGRectMake(([[leftFallingObjects objectAtIndex:i] boundingBox].origin.x - [[leftFallingObjects objectAtIndex:i] contentSize].width/2 - 5), ([[leftFallingObjects objectAtIndex:i] boundingBox].origin.y - 5), ([[leftFallingObjects objectAtIndex:i] contentSize].width + 10), ([[leftFallingObjects objectAtIndex:i]contentSize].height + 10));
        
        
        if ((CGRectContainsPoint(Box, pos)) )
        {
            ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
            [arrayEntry removeObjectAtIndex:i];
            score = score + 10;
        }
        else
        {
            if (([[arrayEntry objectAtIndex:i] boundingBox].origin.y < -5) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > -6.5))
            {
                score = score - 10;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:253 green:211 blue:140];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
                
            }
        }

    }
}



@end
