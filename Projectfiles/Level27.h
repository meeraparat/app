//
//  Level27.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/8/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"
#import "Level20.h"

@interface Level27 : Level20

-(id) initWithLevel27;
+(Level27*) sharedLevel27;

@end
