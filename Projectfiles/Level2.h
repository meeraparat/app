//
//  Level2.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 7/31/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"


@interface Level2 : GameLayer


+(Level2*) sharedLevel2;
-(id) initWithLevel2;



@end
