//
//  Level20.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/7/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"

@interface Level20 : GameLayer

-(id) initWithLevel20;
+(Level20*) sharedLevel20;

@end
