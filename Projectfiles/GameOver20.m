//
//  GameOver20.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver20.h"
#import "Level20.h"

@implementation GameOver20

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level20 alloc] initWithLevel20]];
}

@end
