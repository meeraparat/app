//
//  Level28.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/8/13.
//
//

#import "Level28.h"
#import "NextLevel2728.h"
#import "GameOver28.h"
#import "GenericMenuLayer.h"

@implementation Level28

static Level28 * instanceOfLevel28;


//If another class wants to get a reference to this layer, they can by calling this method
+(Level28*) sharedLevel28
{
	NSAssert(instanceOfLevel28 != nil, @"Level28 instance not yet initialized!");
	return instanceOfLevel28;
}

-(id) initWithLevel28
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateTwentyEight:)];
        [self schedule: @selector(randomNumGenerator:) interval:((arc4random() % 5) + 3)];
        [self level28mods];
    }
    return self;
}

-(void) level28mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel28 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(252, 130, 171, 250)];
    [self addChild:colorLayer z:0];
    
    rightButton.position = ccp(-width, -(height * 10));
    leftButton.position = ccp(-width, -(height * 10));
    
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Tap only the color which is displayed" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 28" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(50, (height - 30));
    [self addChild:levelDisplay];
    
}

-(void) updateTwentyEight: (ccTime) dt
{
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[GenericMenuLayer alloc] init]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver28 alloc] init]];
}

-(void) moveFallingObjects: (NSMutableArray*) fallingObjectArray
{
    for(int i = 0; i < [fallingObjectArray count]; i++)
    {
        [[fallingObjectArray objectAtIndex:i] setPosition: ccp([(CCSprite*)[fallingObjectArray objectAtIndex:i] position].x, [(CCSprite*)[fallingObjectArray objectAtIndex:i] position].y - 2.5)];
    }
}

-(void) processRightCollisions: (NSMutableArray*) arrayEntry
{
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if ((([[arrayEntry objectAtIndex:i] tag] == 0) && (isRedLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 1) && (isBlueLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 2) && (isGreenLabel == TRUE)))
        {
            CGRect Box = CGRectMake(([[rightFallingObjects objectAtIndex:i] boundingBox].origin.x - [[rightFallingObjects objectAtIndex:i] contentSize].width/2 - 5), ([[rightFallingObjects objectAtIndex:i] boundingBox].origin.y - 5), ([[rightFallingObjects objectAtIndex:i] contentSize].width + 10), ([[rightFallingObjects objectAtIndex:i]contentSize].height + 10));
            
            if ((CGRectContainsPoint(Box, pos)) )
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
            else
            {
                if (([[arrayEntry objectAtIndex:i] boundingBox].origin.y < -5) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > -6.5))
                {
                    score = score - 10;
                    CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                    CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:252 green:130 blue:171];
                    CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                    [colorLayer runAction: sequence];
                    
                }
            }
        }
    }
}


-(void) processLeftCollisions: (NSMutableArray*) arrayEntry
{
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if ((([[arrayEntry objectAtIndex:i] tag] == 0) && (isRedLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 1) && (isBlueLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 2) && (isGreenLabel == TRUE)))
        {
            CGRect Box = CGRectMake(([[leftFallingObjects objectAtIndex:i] boundingBox].origin.x - [[leftFallingObjects objectAtIndex:i] contentSize].width/2 - 5), ([[leftFallingObjects objectAtIndex:i] boundingBox].origin.y - 5), ([[leftFallingObjects objectAtIndex:i] contentSize].width + 10), ([[leftFallingObjects objectAtIndex:i]contentSize].height + 10));
            
            if ((CGRectContainsPoint(Box, pos)) )
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
            else
            {
                if (([[arrayEntry objectAtIndex:i] boundingBox].origin.y < -5) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > -6.5))
                {
                    score = score - 10;
                    CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                    CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:252 green:130 blue:171];
                    CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                    [colorLayer runAction: sequence];
                    
                }
            }
        }
    }
}

@end
