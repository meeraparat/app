//
//  GameOver12.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver12.h"
#import "Level12.h"

@implementation GameOver12

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level12 alloc] initWithLevel12]];
}

@end
