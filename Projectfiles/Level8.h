//
//  Level8.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/1/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"

@interface Level8 : GameLayer

-(id) initWithLevel8;
+(Level8*) sharedLevel8;

@end
