//
//  GameOver13.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver13.h"
#import "Level13.h"

@implementation GameOver13

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level13 alloc] initWithLevel13]];
}

@end
