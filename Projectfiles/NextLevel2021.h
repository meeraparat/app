//
//  NextLevel2021.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/7/13.
//
//

#import <Foundation/Foundation.h>
#import "NextLevel.h"

@interface NextLevel2021 : NextLevel

-(id) initWithMods;

@end
