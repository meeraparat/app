//
//  Level25.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/8/13.
//
//

#import "Level25.h"
#import "NextLevel2526.h"
#import "GameOver25.h"

@implementation Level25


static Level25 * instanceOfLevel25;


//If another class wants to get a reference to this layer, they can by calling this method
+(Level25*) sharedLevel25
{
	NSAssert(instanceOfLevel25 != nil, @"Level25 instance not yet initialized!");
	return instanceOfLevel25;
}

-(id) initWithLevel25
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateTwentyFive:)];
        [self level25mods];
    }
    return self;
}

-(void) level25mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel25 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(251, 124, 45, 250)];
    [self addChild:colorLayer z:0];
    
    rightButton.position = ccp(-width, -(height * 10));
    leftButton.position = ccp(-width, -(height * 10));
    
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Don't let any touch the ground!" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 25" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(50, (height - 30));
    [self addChild:levelDisplay];
    
    [self removeChild:mainCreature];
    
}


-(void) updateTwentyFive: (ccTime) dt
{
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel2526 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver25 alloc] init]];
}

-(void) moveFallingObjects: (NSMutableArray*) fallingObjectArray
{
    for(int i = 0; i < [fallingObjectArray count]; i++)
    {
        [[fallingObjectArray objectAtIndex:i] setPosition: ccp([(CCSprite*)[fallingObjectArray objectAtIndex:i] position].x, [(CCSprite*)[fallingObjectArray objectAtIndex:i] position].y - 2.5)];
    }
}

-(void) processRightCollisions: (NSMutableArray *) arrayEntry
{
    for (int i = 0; i < [arrayEntry count]; i++)
    {
        CGRect Box = CGRectMake(([[rightFallingObjects objectAtIndex:i] boundingBox].origin.x - [[rightFallingObjects objectAtIndex:i] contentSize].width/2 - 5), ([[rightFallingObjects objectAtIndex:i] boundingBox].origin.y - 5), ([[rightFallingObjects objectAtIndex:i] contentSize].width + 10), ([[rightFallingObjects objectAtIndex:i]contentSize].height + 10));
        
        if ((CGRectContainsPoint(Box, pos)) )
        {
            ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
            [arrayEntry removeObjectAtIndex:i];
            score = score + 10;
        }
        else
        {
            if (([[arrayEntry objectAtIndex:i] boundingBox].origin.y < -5) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > -6.5))
            {
                score = score - 10;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:251 green:124 blue:45];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
                
            }
        }
        
        
    }
}


-(void) processLeftCollisions: (NSMutableArray *) arrayEntry
{
    for (int i = 0; i < [arrayEntry count]; i++)
    {
        CGRect Box = CGRectMake(([[leftFallingObjects objectAtIndex:i] boundingBox].origin.x - [[leftFallingObjects objectAtIndex:i] contentSize].width/2 - 5), ([[leftFallingObjects objectAtIndex:i] boundingBox].origin.y - 5), ([[leftFallingObjects objectAtIndex:i] contentSize].width + 10), ([[leftFallingObjects objectAtIndex:i]contentSize].height + 10));
        
        
        if ((CGRectContainsPoint(Box, pos)) )
        {
            ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
            [arrayEntry removeObjectAtIndex:i];
            score = score + 10;
        }
        else
        {
            if (([[arrayEntry objectAtIndex:i] boundingBox].origin.y < -5) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > -6.5))
            {
                score = score - 10;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:251 green:124 blue:45];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
                
            }
        }
        
    }
}



@end
