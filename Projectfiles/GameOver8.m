//
//  GameOver8.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver8.h"
#import "Level8.h"

@implementation GameOver8

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level8 alloc] initWithLevel8]];
}

@end
