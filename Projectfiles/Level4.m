//
//  Level4.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 7/31/13.
//
//

#import "Level4.h"
#import "NextLevel45.h"
#import "GameOver4.h"

@implementation Level4

static Level4 * instanceOfLevel4;



//If another class wants to get a reference to this layer, they can by calling this method
+(Level4*) sharedLevel4
{
	NSAssert(instanceOfLevel4 != nil, @"Level4 instance not yet initialized!");
	return instanceOfLevel4;
}

-(id) initWithLevel4
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateFour:)];
        [self schedule: @selector(randomNumGenerator:) interval:((arc4random() % 5) + 3)];
        [self level4mods];
    }
    return self;
}

-(void) level4mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel4 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(252, 115, 250, 250)];
    [self addChild:colorLayer z:0];
    
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Click the color displayed above" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 4" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(55, (height - 25));
    [self addChild:levelDisplay];

  
}

-(void) updateFour: (ccTime) dt
{
    [self processRightCollisions:rightFallingObjects];
    [self processLeftCollisions:leftFallingObjects];
    
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel45 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver4 alloc] init]];
}


-(void) randomNumGenerator: (ccTime) dt
{
    //picks a number 0,1,2
    colorDecider = (arc4random() % 3);
    [self displayType];
    
}

-(void) displayType
{
    if (colorDecider == 0)
    {
        [self removeChild:blueLabel];
        [self removeChild:greenLabel];
        [self removeChild:redLabel];
        isBlueLabel = FALSE;
        isGreenLabel = FALSE;
        redLabel = [CCLabelTTF labelWithString:@"Red" fontName:@"Marker Felt" fontSize:30.0f];
        redLabel.position = ccp(width/2, height - 75);
        [self addChild:redLabel z:2];
        isRedLabel = TRUE;
    }
    else if (colorDecider == 1)
    {
        [self removeChild:redLabel];
        [self removeChild:greenLabel];
        [self removeChild:blueLabel];
        isRedLabel = FALSE;
        isGreenLabel = FALSE;
        blueLabel = [CCLabelTTF labelWithString:@"Blue" fontName:@"Marker Felt" fontSize:30.0f];
        blueLabel.position = ccp(width/2, height - 75);
        [self addChild:blueLabel z:2];
        isBlueLabel = TRUE;
    }
    else if (colorDecider == 2)
    {
        [self removeChild:redLabel];
        [self removeChild:blueLabel];
        [self removeChild:greenLabel];
        isRedLabel = FALSE;
        isBlueLabel = FALSE;
        greenLabel = [CCLabelTTF labelWithString:@"Green" fontName:@"Marker Felt" fontSize:30.0f];
        greenLabel.position = ccp(width/2, height - 75);
        [self addChild:greenLabel z:2];
        isGreenLabel = TRUE;
    }
}

-(void) processRightCollisions: (NSMutableArray*) arrayEntry
{
    CGRect RightRect = CGRectMake(rightButton.position.x - width/2, height/2 + rightButton.position.y, rightButton.contentSize.width, rightButton.contentSize.height);
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isRightClicked == TRUE)
        {
            if(([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (RightRect.origin.y  + RightRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (RightRect.origin.y - RightRect.size.height/2 - 5)))
            {
                if ( (([[arrayEntry objectAtIndex:i] tag] == 0) && (isRedLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 1) && (isBlueLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 2) && (isGreenLabel == TRUE)))
                {
                    ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                    [arrayEntry removeObjectAtIndex:i];
                    score = score + 10;
                }
            }
        }
        else
        {
            if ((([[arrayEntry objectAtIndex:i] boundingBox].origin.y) < (RightRect.origin.y - RightRect.size.height/2 - 5)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > (RightRect.origin.y - RightRect.size.height/2 - 6.5)))
            {
                if ( (([[arrayEntry objectAtIndex:i] tag] == 0) && (isRedLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 1) && (isBlueLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 2) && (isGreenLabel == TRUE)))
                {
                    score = score - 5;
                    CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                    CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:252 green:115 blue:250];
                    CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                    [colorLayer runAction: sequence];
                }
            }
        }
    }
    
    isRightClicked = FALSE;
}

-(void) processLeftCollisions: (NSMutableArray*) arrayEntry
{
    
    CGRect LeftRect = CGRectMake(leftButton.position.x - width/2, height/2 + leftButton.position.y, leftButton.contentSize.width, leftButton.contentSize.height);
    
    
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if (isLeftClicked == TRUE)
        {
            if (([[arrayEntry objectAtIndex:i] boundingBoxCenter].y <= (LeftRect.origin.y  + LeftRect.size.height/2)) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y >= (LeftRect.origin.y - LeftRect.size.height/2 - 5)))
            {
                if ( (([[arrayEntry objectAtIndex:i] tag] == 0) && (isRedLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 1) && (isBlueLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 2) && (isGreenLabel == TRUE)))
                {
                    ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                    [arrayEntry removeObjectAtIndex:i];
                    score = score + 10;
                }
            }
        }
        else
        {
            if ([[arrayEntry objectAtIndex:i] boundingBox].origin.y < (LeftRect.origin.y - LeftRect.size.height/2 - 5) && [[arrayEntry objectAtIndex:i] boundingBox].origin.y > ((LeftRect.origin.y - LeftRect.size.height/2) - 6.5))
            {
                if ( (([[arrayEntry objectAtIndex:i] tag] == 0) && (isRedLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 1) && (isBlueLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 2) && (isGreenLabel == TRUE)))
                {
                    score = score - 5;
                    CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                    CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:252 green:115 blue:250];
                    CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                    [colorLayer runAction: sequence];
                }
            }
        }
    }
    
    isLeftClicked = FALSE;
}



@end
