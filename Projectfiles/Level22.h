//
//  Level22.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/7/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"

@interface Level22 : GameLayer
{
    CCSprite * spider1;
    CCSprite * spider2;
    CCSprite * spider3;
    CCSprite * spider4;
    CCSprite * spider5;
    CCSprite * spider6;
    CCSprite * spider7;
    CCSprite * spider8;
    CCSprite * spider9;
    CCSprite * spider10;

    NSMutableArray * spiderArrayDown;
    NSMutableArray * spiderArrayLeft;
    NSMutableArray * spiderArrayUp;
    NSMutableArray * spiderArrayRight;
}

-(id) initWithLevel22;
+(Level22*) sharedLevel22;

@end
