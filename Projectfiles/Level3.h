//
//  Level3.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 7/31/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"


@interface Level3 : GameLayer
{
    
}


+(Level3*) sharedLevel3;
-(id) initWithLevel3;


@end
