//
//  Level12.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/2/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"
#import "Level6.h"

@interface Level12 : Level6
{
//    CCSprite * babyRight;
//    CCSprite * babyLeft;
//    CCSprite * alligatorRight;
//    CCSprite * alligatorLeft;
//    
//    NSMutableArray * babyArrayRight;
//    NSMutableArray * babyArrayLeft;
}

-(id) initWithLevel12;
+(Level12*) sharedLevel12;

@end
