//
//  Level11.h
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/1/13.
//
//

#import <Foundation/Foundation.h>
#import "GameLayer.h"
#import "Level8.h"

@interface Level11 : Level8

-(id) initWithLevel11;
+(Level11*) sharedLevel11;

@end
