//
//  FallingObjects.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 7/17/13.
//
//

#import "FallingObjects.h"
#import "GameLayer.h"

@implementation FallingObjects

+(id) getType: (int) type
{
    id fObject = [[self alloc] initWithObjectImage: type];
    
    return fObject;
}

-(id) initWithObjectImage:(int) type
{
    if (type == 0)
    {
        
        if (self = [super initWithFile:@"red_dot.jpeg"])
        {
            //can set any boolean, or anything, etc
        }
    }
    
    else if (type == 1)
    {
        if (self = [super initWithFile:@"blue_dot.png"])
        {
            
        }
    }
    
    else if (type == 2)
    {
        if (self = [super initWithFile:@"green_dot.png"])
        {
            
        }
    }
    
    return self;
    
}


@end
