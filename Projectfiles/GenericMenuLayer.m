//
//  GenericMenuLayer.m
//  Game Template
//
//  Created by Jeremy Rossmann on 7/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GenericMenuLayer.h"
#import "GameLayer.h"
#import "GameOverLayer.h"
#import "Tutorial.h"

@implementation GenericMenuLayer
{
    CCNode *startTitleLabel;
    CCMenu *startMenu;
    CCMenuItem *startButton;
    CCMenuItem *tutorialButton;
    CGPoint startTitleLabelTargetPoint;
}


-(id) init
{
	if ((self = [super init]))
	{
        // set background color: transparent color
        CCLayerColor* colorLayer = [CCLayerColor layerWithColor:ccc4(50, 220, 183, 200)];
        [self addChild:colorLayer z:0];
        
        //        //setup the start menu title
        //        if (FALSE) {
        //            // OPTION 1: Title as Text
        //            CCLabelTTF *tempStartTitleLabel = [CCLabelTTF labelWithString:@"Good Job!"
        //                                                                 fontName:@"Arial"
        //                                                                 fontSize:20];
        //            tempStartTitleLabel.color = ccc3(255, 255, 255);
        //            startTitleLabel = tempStartTitleLabel;
        //        } else {
        //            // OPTION 2: Title as Sprite
        CCSprite *startLabelSprite = [CCSprite spriteWithFile:@"title.png"];
        startTitleLabel = startLabelSprite;
        //        }
        
        CGPoint screenCenter = [CCDirector sharedDirector].screenCenter;
        CGSize screenSize = [CCDirector sharedDirector].screenSize;
        
        // place the startTitleLabel off-screen, later we will animate it on screen
        startTitleLabel.position = ccp (screenCenter.x, screenSize.height + 100);
        
        // this will be the point, we will animate the title to
        startTitleLabelTargetPoint = ccp(screenCenter.x, screenSize.height - 80);
        
		[self addChild:startTitleLabel];
        
        // add a start button
        CCSprite *normalStartButton = [CCSprite spriteWithFile:@"playbutton.png"];
        CCSprite *selectedStartButton = [CCSprite spriteWithFile:@"playbutton.png"];
        startButton = [CCMenuItemSprite itemWithNormalSprite:normalStartButton selectedSprite:selectedStartButton block:^(id sender) {
            
            //called once Play button is pressed
            CCMoveTo *moveOffScreen = [CCMoveTo actionWithDuration:.6f position:ccp(self.position.x, self.contentSize.height * 2)];
            
            CCAction *movementCompleted = [CCCallBlock actionWithBlock:^{
                // cleanup
                //MainMenuLayer screen disappears
                self.visible = FALSE;
                [[CCDirector sharedDirector] replaceScene: [[GameLayer alloc] init]];
                //this layer calls its parent (gameplaylayer) and calls a specific method in it
                //                [(GameLayer*)[self parent] startGame];
                //this layer gets trashed
                [self removeFromParent];
            }];
            
            CCSequence *menuHideMovement = [CCSequence actions:moveOffScreen, movementCompleted, nil];
            [self runAction:menuHideMovement];
            
        }];
        
        CCSprite * normalTutorialButton = [CCSprite spriteWithFile:@"helpButton.png"];
        CCSprite * selectedTutorialButton = [CCSprite spriteWithFile:@"helpButton.png"];
        tutorialButton = [CCMenuItemSprite itemWithNormalSprite:normalTutorialButton selectedSprite:selectedTutorialButton block:^(id sender)
                          {
                              [[CCDirector sharedDirector] replaceScene: [[Tutorial alloc] init]];
                          }];
                                  
        startMenu = [CCMenu menuWithItems:startButton, tutorialButton, nil];
        startMenu.position = ccp(screenCenter.x, screenCenter.y - 25);
        [startMenu alignItemsVertically];
        [self addChild: startMenu];
	}
    
	return self;
}

#pragma mark - Scene Lifecyle

/**
 This method is called when the scene becomes visible. You should add any code, that shall be executed once
 the scene is visible, to this method.
 */
-(void)onEnterTransitionDidFinish
{
    [super onEnterTransitionDidFinish];
    
    // animate the title on to screen
    CCMoveTo *move = [CCMoveTo actionWithDuration:1.f position:startTitleLabelTargetPoint];
    id easeMove = [CCEaseBackInOut actionWithAction:move];
    [startTitleLabel runAction: easeMove];
    
    
}

-(void) goToTutorial: (CCMenuItemImage *) tutorialButton
{
    [[CCDirector sharedDirector] replaceScene: [[Tutorial alloc] init]];
}

@end
        
        
        
//        
//        CCLabelTTF *label = [CCLabelTTF labelWithString:@"Tap Me!" fontName:@"arial" fontSize:40.0f];
//        
//        CCMenuItemLabel *item = [CCMenuItemLabel itemWithLabel:label target:self selector:@selector(doSomething)];
//
//                
//        CCMenu *menu = [CCMenu menuWithItems:item, nil];
//        
//        [self addChild:menu];
//        
//    }
//    
//    return self;
//    
//}
//
//-(void) doSomething
//{
//        [[CCDirector sharedDirector] replaceScene: [[GameLayer alloc] init]];
//}
//
