//
//  Level27.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/8/13.
//
//

#import "Level27.h"
#import "NextLevel2728.h"
#import "GameOver27.h"

@implementation Level27


static Level27 * instanceOfLevel27;


//If another class wants to get a reference to this layer, they can by calling this method
+(Level27*) sharedLevel27
{
	NSAssert(instanceOfLevel27 != nil, @"Level27 instance not yet initialized!");
	return instanceOfLevel27;
}

-(id) initWithLevel27
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateTwentySeven:)];
        [self level27mods];
    }
    return self;
}

-(void) level27mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel27 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(223, 252, 105, 250)];
    [self addChild:colorLayer z:0];
    
    rightButton.position = ccp(-width, -(height * 10));
    leftButton.position = ccp(-width, -(height * 10));
    
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Tap only the Red and Blue" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 27" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(50, (height - 30));
    [self addChild:levelDisplay];
    
}

-(void) updateTwentySeven: (ccTime) dt
{
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel2728 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver27 alloc] init]];
}

-(void) moveFallingObjects: (NSMutableArray*) fallingObjectArray
{
    for(int i = 0; i < [fallingObjectArray count]; i++)
    {
        [[fallingObjectArray objectAtIndex:i] setPosition: ccp([(CCSprite*)[fallingObjectArray objectAtIndex:i] position].x, [(CCSprite*)[fallingObjectArray objectAtIndex:i] position].y - 2.5)];
    }
}

-(void) processRightCollisions: (NSMutableArray *) arrayEntry
{
    for (int i = 0; i < [arrayEntry count]; i++)
    {
        if (([[arrayEntry objectAtIndex:i] tag] == 0) || ([[arrayEntry objectAtIndex:i] tag] == 1))
        {
            CGRect Box = CGRectMake(([[rightFallingObjects objectAtIndex:i] boundingBox].origin.x - [[rightFallingObjects objectAtIndex:i] contentSize].width/2 - 5), ([[rightFallingObjects objectAtIndex:i] boundingBox].origin.y - 5), ([[rightFallingObjects objectAtIndex:i] contentSize].width + 10), ([[rightFallingObjects objectAtIndex:i]contentSize].height + 10));
            
            
            if ((CGRectContainsPoint(Box, pos)) )
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else if ([[arrayEntry objectAtIndex:i] tag] == 2)
        {
            CGRect Box = CGRectMake(([[rightFallingObjects objectAtIndex:i] boundingBox].origin.x - [[rightFallingObjects objectAtIndex:i] contentSize].width/2), ([[rightFallingObjects objectAtIndex:i] boundingBox].origin.y), ([[rightFallingObjects objectAtIndex:i] contentSize].width), ([[rightFallingObjects objectAtIndex:i]contentSize].height));
            
            if (CGRectContainsPoint(Box, pos))
            {
                score = score - 10;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:223 green:252 blue:105];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];
            }
        }
    }
}


-(void) processLeftCollisions: (NSMutableArray *) arrayEntry
{
    for (int i = 0; i < [arrayEntry count]; i++)
    {
        if (([[arrayEntry objectAtIndex:i] tag] == 0) || ([[arrayEntry objectAtIndex:i] tag] == 1))
        {
            CGRect Box = CGRectMake(([[leftFallingObjects objectAtIndex:i] boundingBox].origin.x - [[leftFallingObjects objectAtIndex:i] contentSize].width/2 - 5), ([[leftFallingObjects objectAtIndex:i] boundingBox].origin.y - 5), ([[leftFallingObjects objectAtIndex:i] contentSize].width + 10), ([[leftFallingObjects objectAtIndex:i]contentSize].height + 10));
            
            
            if ((CGRectContainsPoint(Box, pos)) )
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
        }
        else if ([[arrayEntry objectAtIndex:i] tag] == 2)
        {
            CGRect Box = CGRectMake(([[leftFallingObjects objectAtIndex:i] boundingBox].origin.x - [[leftFallingObjects objectAtIndex:i] contentSize].width/2), ([[leftFallingObjects objectAtIndex:i] boundingBox].origin.y), ([[leftFallingObjects objectAtIndex:i] contentSize].width), ([[leftFallingObjects objectAtIndex:i]contentSize].height));
            
            if ((CGRectContainsPoint(Box, pos)))
            {
                score = score - 10;
                CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:223 green:252 blue:105];
                CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                [colorLayer runAction: sequence];            }
        }
    }
}


@end
