//
//  GameOver4.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver4.h"
#import "Level4.h"

@implementation GameOver4

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level4 alloc] initWithLevel4]];
}

@end
