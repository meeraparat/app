//
//  GameOver26.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver26.h"
#import "Level26.h"

@implementation GameOver26

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level26 alloc] initWithLevel26]];
}

@end
