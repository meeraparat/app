//
//  Level23.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/7/13.
//
//

#import "Level23.h"
#import "NextLevel2324.h"
#import "GameOver23.h"

@implementation Level23


static Level23 * instanceOfLevel23;


//If another class wants to get a reference to this layer, they can by calling this method
+(Level23*) sharedLevel23
{
	NSAssert(instanceOfLevel23 != nil, @"Level23 instance not yet initialized!");
	return instanceOfLevel23;
}

-(id) initWithLevel23
{
    if ((self = [super init]))
    {
        [self schedule:@selector(updateTwentyThree:)];
        [self schedule: @selector(randomNumGenerator:) interval:((arc4random() % 5) + 3)];
        [self level23mods];
    }
    return self;
}

-(void) level23mods
{
    //this line initializes the instanceOfLevel2 variable such that it can be accessed by the sharedLevel1 method
    instanceOfLevel23 = self;
    
    colorLayer = [CCLayerColor layerWithColor:ccc4(190, 25, 250, 250)];
    [self addChild:colorLayer z:0];
    
    rightButton.position = ccp(-width, -(height * 10));
    leftButton.position = ccp(-width, -(height * 10));
    
    CCLabelTTF * hint = [CCLabelTTF labelWithString:@"Only click on the appropriate color" fontName:@"Marker Felt" fontSize:20.0f];
    hint.position = ccp(width/2, 25);
    [self addChild:hint z:1];
    
    CCLabelTTF * levelDisplay = [CCLabelTTF labelWithString:@"Level 23" fontName:@"Marker Felt" fontSize:20];
    levelDisplay.position = ccp(50, (height - 30));
    [self addChild:levelDisplay];
    
}


-(void) updateTwentyThree: (ccTime) dt
{    
    if (score >= 100)
    {
        [[CCDirector sharedDirector] replaceScene: [[NextLevel2324 alloc] initWithMods]];
    }
}

-(void) switchScene
{
    [[CCDirector sharedDirector] replaceScene: [[GameOver23 alloc] init]];
}

-(void) randomNumGenerator: (ccTime) dt
{
    //picks a number 0,1,2
    colorDecider = (arc4random() % 3);
    [self displayType];
    
}

-(void) displayType
{
    if (colorDecider == 0)
    {
        [self removeChild:blueLabel];
        [self removeChild:greenLabel];
        [self removeChild:redLabel];
        isBlueLabel = FALSE;
        isGreenLabel = FALSE;
        redLabel = [CCLabelTTF labelWithString:@"Red" fontName:@"Marker Felt" fontSize:30.0f];
        redLabel.position = ccp(width/2, height - 75);
        [self addChild:redLabel z:2];
        isRedLabel = TRUE;
    }
    else if (colorDecider == 1)
    {
        [self removeChild:redLabel];
        [self removeChild:greenLabel];
        [self removeChild:blueLabel];
        isRedLabel = FALSE;
        isGreenLabel = FALSE;
        blueLabel = [CCLabelTTF labelWithString:@"Blue" fontName:@"Marker Felt" fontSize:30.0f];
        blueLabel.position = ccp(width/2, height - 75);
        [self addChild:blueLabel z:2];
        isBlueLabel = TRUE;
    }
    else if (colorDecider == 2)
    {
        [self removeChild:redLabel];
        [self removeChild:blueLabel];
        [self removeChild:greenLabel];
        isRedLabel = FALSE;
        isBlueLabel = FALSE;
        greenLabel = [CCLabelTTF labelWithString:@"Green" fontName:@"Marker Felt" fontSize:30.0f];
        greenLabel.position = ccp(width/2, height - 75);
        [self addChild:greenLabel z:2];
        isGreenLabel = TRUE;
    }
}

-(void) processRightCollisions: (NSMutableArray*) arrayEntry
{
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if ((([[arrayEntry objectAtIndex:i] tag] == 0) && (isRedLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 1) && (isBlueLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 2) && (isGreenLabel == TRUE)))
        {
            CGRect Box = CGRectMake(([[rightFallingObjects objectAtIndex:i] boundingBox].origin.x - [[rightFallingObjects objectAtIndex:i] contentSize].width/2 - 5), ([[rightFallingObjects objectAtIndex:i] boundingBox].origin.y - 5), ([[rightFallingObjects objectAtIndex:i] contentSize].width + 10), ([[rightFallingObjects objectAtIndex:i]contentSize].height + 10));
            
            if ((CGRectContainsPoint(Box, pos)) )
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
            else
            {
                if (([[arrayEntry objectAtIndex:i] boundingBox].origin.y < -5) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > -6.5))
                {
                    score = score - 10;
                    CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                    CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:190 green:25 blue:250];
                    CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                    [colorLayer runAction: sequence];
                    
                }
            }
        }
    }
}


-(void) processLeftCollisions: (NSMutableArray*) arrayEntry
{
    for(int i = 0; i < [arrayEntry count]; i++)
    {
        if ((([[arrayEntry objectAtIndex:i] tag] == 0) && (isRedLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 1) && (isBlueLabel == TRUE)) || (([[arrayEntry objectAtIndex:i] tag] == 2) && (isGreenLabel == TRUE)))
        {
            CGRect Box = CGRectMake(([[leftFallingObjects objectAtIndex:i] boundingBox].origin.x - [[leftFallingObjects objectAtIndex:i] contentSize].width/2 - 5), ([[leftFallingObjects objectAtIndex:i] boundingBox].origin.y - 5), ([[leftFallingObjects objectAtIndex:i] contentSize].width + 10), ([[leftFallingObjects objectAtIndex:i]contentSize].height + 10));
            
            if ((CGRectContainsPoint(Box, pos)) )
            {
                ((CCSprite*)[arrayEntry objectAtIndex:i]).visible = FALSE;
                [arrayEntry removeObjectAtIndex:i];
                score = score + 10;
            }
            else
            {
                if (([[arrayEntry objectAtIndex:i] boundingBox].origin.y < -5) && ([[arrayEntry objectAtIndex:i] boundingBox].origin.y > -6.5))
                {
                    score = score - 10;
                    CCTintTo * tintto = [CCTintTo actionWithDuration:0.1f red:255 green:0 blue:0];
                    CCTintTo * tintback = [CCTintTo actionWithDuration:0.2f red:190 green:25 blue:250];
                    CCSequence * sequence = [CCSequence actions: tintto, tintback, nil];
                    [colorLayer runAction: sequence];
                    
                }
            }
        }
    }
}

@end
