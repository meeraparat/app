//
//  GameOver21.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver21.h"
#import "Level21.h"

@implementation GameOver21

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level21 alloc] initWithLevel21]];
}

@end
