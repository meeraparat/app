//
//  GameOverLayer.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 7/11/13.
//
//


#import "GenericMenuLayer.h"
#import "GameLayer.h"
#import "GameOverLayer.h"

@implementation GameOverLayer

static GameOverLayer* instanceOfGameOverLayer;


//If another class wants to get a reference to this layer, they can by calling this method
+(GameOverLayer*) sharedGameOverLayer
{
	NSAssert(instanceOfGameOverLayer != nil, @"GameOverLayer instance not yet initialized!");
	return instanceOfGameOverLayer;
}

-(id) init
{
	if ((self = [super init]))
	{
        //this line initializes the instanceOfGameOverLayer variable such that it can be accessed by the sharedGameLayer method
		instanceOfGameOverLayer = self;
        
        width = [GameLayer screenRect].size.width;
        height = [GameLayer screenRect].size.height;
        
        CCLayerColor* colorLayer = [CCLayerColor layerWithColor:ccc4(22, 100, 99, 250)];
        [self addChild:colorLayer z:0];
        
        CCLabelTTF * gameOver = [CCLabelTTF labelWithString:@"Game Over" fontName:@"Marker Felt" fontSize:40.0f];
        //gameOver.position = ccp(0, -(height/4));
        
        CCLabelTTF * replay = [CCLabelTTF labelWithString:@"Replay" fontName:@"Marker Felt" fontSize:20.0f];
        //replay.position = ccp(0, 0);
        
        CCMenuItemLabel * item1 = [CCMenuItemLabel itemWithLabel:gameOver];
        CCMenuItemLabel * item2 = [CCMenuItemLabel itemWithLabel:replay target:self selector:@selector(replayGame)];
        
        CCMenu * menu = [CCMenu menuWithItems:item1, item2, nil];
        [menu alignItemsVertically];
        
        [self addChild:menu];
        
        //DISPLAY SCORE
        
    }
    
    return self;
    
}

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[GameLayer alloc] init]];
}


-(void) dealloc
{
	instanceOfGameOverLayer = nil;
	
#ifndef KK_ARC_ENABLED
	// don't forget to call "super dealloc"
	[super dealloc];
#endif // KK_ARC_ENABLED
}


@end
