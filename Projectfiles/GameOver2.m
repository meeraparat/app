//
//  GameOver2.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver2.h"
#import "Level2.h"

@implementation GameOver2


-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level2 alloc] initWithLevel2]];
}


@end
