//
//  GameOver28.m
//  Empty-Game-Portrait
//
//  Created by Meera Parat on 8/12/13.
//
//

#import "GameOver28.h"
#import "Level28.h"

@implementation GameOver28

-(void) replayGame
{
    [[CCDirector sharedDirector] replaceScene: [[Level28 alloc] initWithLevel28]];
}

@end
