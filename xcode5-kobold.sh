#!/bin/bash

if [ -d ~/Kobold2D/Kobold2D-2.1.0/ ]
then
  cd ~/Kobold2D/Kobold2D-2.1.0/__Kobold2D__/
elif [ -d /Kobold2D/Kobold2D-2.1.0/ ]
then
  cd /Kobold2D/Kobold2D-2.1.0/__Kobold2D__/
else
  echo "Could not find Kobold2D-2.1.0 folder. If you're using an older version of Kobold2D, please upgrade. If you are using Kobold2D 2.1.0 and are seeing this error, please email ashu@makegameswith.us for help"
  exit 1
fi

curl https://s3.amazonaws.com/mgwu-misc/xcode5-kobold.patch > xcode5-kobold.patch
git apply --whitespace=nowarn xcode5-kobold.patch
rm xcode5-kobold.patch